import { Request, Response } from 'express';
import paymentService from '../services/PaymentService';

class PaymentController {

  public async list(req: Request, res: Response) {

    const options = {
      page: parseInt(req.query.page),
      pageSize: parseInt(req.query.pageSize),
      params: {
      },
      populate: 'clientId',
    };

    if (req.query.clientId) { Object.assign(options.params, { clientId: req.query.clientId }); }
    if (req.query.status) { Object.assign(options.params, { status: req.query.status }); }

    const payments = await paymentService.find({ ...options });

    res.json({
      message: 'Sucesso ao buscar pagamentos',
      payments,
    });

  }

};

const paymentController = new PaymentController();
export default paymentController;
