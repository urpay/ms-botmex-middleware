import { ClientInterface } from '../../models/Client';
import { Request, Response } from 'express';

import * as returnMessages from '../../config/returnMessages';
import * as otplib from 'otplib';

import qrcode from 'qrcode';

import DbRedis from '../../config/database/redis';
import { envAppName } from '../../config/environment';
import ErrorLib from '../../lib/ErrorLib';

class TwoFactorsController {

  public async getSecret(req: Request, res: Response) {

    const clientFound: ClientInterface = res.locals.clientFound;

    if (clientFound.password.twoFactors.isActive) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eTwoFactorsIsActive,
        errorCode: 10,
      });
    }

    if (!clientFound.password.twoFactors.secret || req.query.reset) {
      clientFound.set('password.twoFactors.secret', otplib.authenticator.generateSecret());
      await clientFound.save();
    }

    res.json({
      secret: clientFound.password.twoFactors.secret,
      qrCode: await qrcode.toDataURL(otplib.authenticator.keyuri(clientFound.email.email, envAppName, clientFound.password.twoFactors.secret)),
    });
  }

  public setActive = async (req: Request, res: Response) => {

    const clientFound: ClientInterface = res.locals.clientFound;

    if (clientFound.password.twoFactors.isActive) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eTwoFactorsIsActive,
        errorCode: 10,
      });
    }

    if (!clientFound.password.twoFactors.secret) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eTwoFactorsNoSecret,
        errorCode: 11,
      });
    }

    if (!otplib.authenticator.check(req.body.code2fa, clientFound.password.twoFactors.secret)) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eTwoFactorsInvalid,
        errorCode: 9,
        httpCode: 401,
      });
    }

    await this.setLastUsed(clientFound, req.body.code2fa);

    clientFound.set('password.twoFactors.isActive', true);
    await clientFound.save();

    res.json({ message: returnMessages.ptBr.twoFactorsActived });
  }

  public async setLastUsed(client: ClientInterface, code: string) {
    await DbRedis.getClient().set(`last-two-factors::client::${client._id}`, code, 'ex', 30);
  }

  public async isLastUsed(client: ClientInterface, code: string) {
    const lastUsed = await DbRedis.getClient().get(`last-two-factors::client::${client._id}`);

    if (lastUsed == code)
      return true;

    return false;
  }

}

export default new TwoFactorsController();
