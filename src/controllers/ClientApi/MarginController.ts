import { Client, ClientInterface } from '../../models/Client';

import { Request, Response } from 'express';

import * as returnMessages from '../../config/returnMessages';
import bitMexLib from '../../lib/BitMexLib';

class MarginController {

  public async userMargin(req: Request, res: Response) {
  
    const clientFound: ClientInterface = await Client.findById(res.locals.clientFound).exec();

    if (!clientFound) {
      res.status(422).send({ message: returnMessages.ptBr.eClientNotFound });
      return;
    }

    const margin = await bitMexLib.getUserMargin(clientFound);

    res.json({
      message: 'Sucesso ao buscar a margem do usuário',
      margin,
    });
    
  }

};

const marginController = new MarginController();
export default marginController;
