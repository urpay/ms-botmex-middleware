import { Client, ClientInterface } from '../../models/Client';

import { Request, Response } from 'express';

import * as returnMessages from '../../config/returnMessages';
import { CancelOrder } from '../../models/CancelOrder';
import ErrorLib from '../../lib/ErrorLib';

class CancelOrderController {

  public async setStopLoss(req: Request, res: Response) {

    const { stopLoss, isActive } = req.body;

    const clientFound: ClientInterface = await Client.findById(res.locals.clientFound).exec();

    if (!clientFound) {
      res.status(422).send({ message: returnMessages.ptBr.eClientNotFound });
      return;
    }

    const foundCancelOrder = await CancelOrder.findOne({ clientId: clientFound._id });

    if (!foundCancelOrder) {
      const cancelOrder = await CancelOrder.create({
        clientId: clientFound._id,
        stopLoss,
        isActive,
      });
      res.json({
        cancelOrder,
      });
    }


    await foundCancelOrder.update({
      stopLoss,
      isActive,
    });

    res.json({
      cancelOrder: foundCancelOrder,
    });


  }


  public async getStopLoss(req: Request, res: Response) {

    const clientFound: ClientInterface = await Client.findById(res.locals.clientFound).exec();

    if (!clientFound) {
      res.status(422).send({ message: returnMessages.ptBr.eClientNotFound });
      return;
    }

    const cancelOrder = await CancelOrder.findOne({ clientId: clientFound._id }).exec();

    if (!cancelOrder) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eCancelOrderNotFoud,
      });
    }

    res.json({
      cancelOrder,
    });

  }

};

const cancelOrderController = new CancelOrderController();
export default cancelOrderController;
