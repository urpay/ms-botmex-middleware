
import { Request, Response } from 'express';
import * as returnMessages from '../../config/returnMessages';
import paymentService from '../../services/PaymentService';
import ErrorLib from '../../lib/ErrorLib';
import { ClientInterface, Client } from '../../models/Client';

class PaymentController {

  public async list(req: Request, res: Response) {
    const clientFound: ClientInterface = await Client.findById(res.locals.clientFound).exec();

    if (!clientFound) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eClientNotFound,
      });

    }

    const options = {
      page: parseInt(req.query.page),
      pageSize: parseInt(req.query.pageSize),
      params: {
        clientId: res.locals.clientFound,
      },
    };

    if (req.query.status) { Object.assign(options.params, { status: req.query.status }); }

    const payments = await paymentService.find({ ...options });

    res.json({
      message: 'Sucesso ao buscar pagamentos',
      payments,
    });

  }

};

const paymentController = new PaymentController();
export default paymentController;
