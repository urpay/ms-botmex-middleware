import { Client, ClientInterface } from '../../models/Client';

import { Request, Response } from 'express';

import * as returnMessages from '../../config/returnMessages';
import bitMexLib from '../../lib/BitMexLib';
import walletTransactionService from '../../services/WalletTransactionService';
import { WalletTransaction } from '../../models/WalletTransaction';
import { lastDayOfMonth, format, parseISO, getMonth, getYear } from 'date-fns';
import { months } from '../../config/returnMonths';

class WalletController {
  public async walletHistory(req: Request, res: Response) {
    const clientFound: ClientInterface = await Client.findById(res.locals.clientFound).exec();

    if (!clientFound) {
      res.status(422).send({ message: returnMessages.ptBr.eClientNotFound });
      return;
    }
    
    let initialDate;
    let finalDate;
    if (req?.query?.initialDate && req?.query?.finalDate) {
      initialDate = format(parseISO(req.query.initialDate), 'yyyy-MM-dd');
      finalDate = format(parseISO(req.query.finalDate), 'yyyy-MM-dd');
    } else {
      initialDate = format(new Date().setDate(1), 'yyyy-MM-dd');
      finalDate = format(lastDayOfMonth(new Date()), 'yyyy-MM-dd');
    }

    const allWalletHistory = await walletTransactionService.find({
      params: {
        clientId: clientFound._id,
        transactTime: { $gte: initialDate + 'T00:00:00', $lte: finalDate + 'T23:59:59' },
      },
      sort: { transactTime: -1 },
    });

    res.json({ documents: allWalletHistory });
  }

  public async openWalletTranscation(req: Request, res: Response) {

    const clientFound: ClientInterface = await Client.findById(res.locals.clientFound).exec();

    if (!clientFound) {
      res.status(422).send({ message: returnMessages.ptBr.eClientNotFound });
      return;
    }

    const response = await bitMexLib.getWalletHistory(clientFound, { count: 20 });

    const openList = response.filter((transaction: any) => transaction.transactID == '00000000-0000-0000-0000-000000000000');
    const transaction = response.find((transaction: any) => (transaction.transactID == '00000000-0000-0000-0000-000000000000' && transaction.transactType == 'RealisedPNL'));


    res.json({
      message: 'Transação encontrada',
      transaction,
      openList,
    });

  }



  public async amountMonth(req: Request, res: Response) {

    const clientFound: ClientInterface = await Client.findById(res.locals.clientFound).exec();

    if (!clientFound) {
      res.status(422).send({ message: returnMessages.ptBr.eClientNotFound });
      return;
    }

    const firstDayCurrentMonth = format(new Date().setDate(1), 'yyyy-MM-dd');
    const lastDayCurrentMonth = format(lastDayOfMonth(new Date()), 'yyyy-MM-dd');


    console.log({
      $match: {
        clientId: clientFound._id,
        transactTime: { $gte: new Date(firstDayCurrentMonth + 'T03:00:00'), $lte: new Date(lastDayCurrentMonth + 'T03:00:00') },
        transactType: 'RealisedPNL',
      },
    });
    const amountMonth = await WalletTransaction.aggregate([
      {
        $match: {
          clientId: clientFound._id,
          transactTime: { $gte: new Date(firstDayCurrentMonth + 'T03:00:00'), $lte: new Date(lastDayCurrentMonth + 'T23:59:59') },
          transactType: 'RealisedPNL',
        },
      },
      {
        $group: {
          _id: '$clientId',
          amountMonth: {
            $sum: '$amount',
          },
        },
      }]);
    const [amount] = amountMonth;

    res.json({
      amountMonth,
      amount,
    });

  }

  public async getOperationsMonths(req: Request, res: Response): Promise<Response> {
    const { year } = req.query;
    const clientFound: ClientInterface = await Client.findById(res.locals.clientFound).exec();

    const allWalletHistory = await walletTransactionService.find({
      params: {
        clientId: clientFound._id,
        transactTime: { $gte: `${year}-01-01T00:00:00`, $lte: `${year}-12-31T23:59:59` },
      },
      sort: { transactTime: -1 },
    });

    const { documents } = allWalletHistory;

    let numericMonths = documents.map((document) => getMonth(document.transactTime) + 1);

    numericMonths = [ ...new Set(numericMonths) ];

    const extendedMonths = months.map((month: string, index: number) => {      
      return {
        month,
        hasOperations: numericMonths.includes(index + 1),
      };
    });

    return res.status(200).send({ operationsMonths: extendedMonths });
  }

  public async getOperationsYears(req: Request, res: Response): Promise<Response> {
    const clientFound: ClientInterface = await Client.findById(res.locals.clientFound).exec();

    const allWalletHistory = await walletTransactionService.find({
      params: {
        clientId: clientFound._id,
      },
      sort: { transactTime: -1 },
    });

    const { documents } = allWalletHistory;

    let years = documents.map((document) => getYear(document.transactTime));

    years = [ ...new Set(years) ];

    return res.status(200).send({ operationsYear: years });
  }
};

const walletController = new WalletController();
export default walletController;
