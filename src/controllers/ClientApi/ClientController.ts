import { Client, ClientInterface } from '../../models/Client';

import { Request, Response } from 'express';

import * as returnMessages from '../../config/returnMessages';

import clientService from '../../services/ClientService';

class ClientController {



  public async show(req: Request, res: Response) {


    const clientFound: ClientInterface = await Client.findById(res.locals.clientFound).exec();

    if (!clientFound) {
      res.status(422).send({ message: returnMessages.ptBr.eClientNotFound });
      return;
    }

    res.json({ client: clientFound.getReturnJson() });
  }


  
  public async update(req: Request, res: Response) {
    const loggedUser: ClientInterface = res.locals.clientFound;

    const client = await clientService.update(loggedUser._id, {
      name: req.body.name,
      telegramId: req.body.telegramId,
      apiKey: req.body.apiKey,
      apiSecret: req.body.apiSecret,
    });

    res.json({ client });
  }

  public async updatePassword(req: Request, res: Response) {
    const clientFound: ClientInterface = await Client.findById(res.locals.clientFound).exec();

    if (!clientFound) {
      res.status(422).send({ message: returnMessages.ptBr.eClientNotFound });
      return;
    }


    await clientFound.updatePassword(req.body.password, req.body.passwordConfirm);

    res.json({ message: returnMessages.ptBr.passwordChanged });
  }

};

const clientController = new ClientController();
export default clientController;
