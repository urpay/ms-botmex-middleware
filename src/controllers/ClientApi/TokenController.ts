import dbRedis from '../../config/database/redis';
import { Client, ClientInterface } from '../../models/Client';

import { sign } from 'jsonwebtoken';
import {  envTokenClient } from '../../config/environment';
import { Request, Response } from 'express';

import * as returnMessages from '../../config/returnMessages';

import * as Yup from 'yup';
import passwordLib from '../../lib/PasswordLib';
import ErrorLib from '../../lib/ErrorLib';

class ClientTokenController {

  public generateTokenLoginRoute = async (req: Request, res: Response) => {
    const yupSchema = Yup.object().shape({
      email: Yup.string().required(),
      password: Yup.string().required(),
    });

    if (!await yupSchema.isValid(req.body)) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eDataNotSent,
        errorCode: 0,
      });
    }

    const finded = await Client.findOne({ 'email.email': req.body.email }, '+password.password').exec();


    if (!finded) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eDataNotSent,
        errorCode: 4,
      });
    }

    if (!passwordLib.checkHash(req.body.password, finded.password.password)) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eDataNotSent,
        errorCode: 4,
      });
    }


    const returnToken = await this.generateTokenLogin(finded);

    res.json({
      user: finded.getReturnJson(),
      token: returnToken,
    });
  }

  public async generateTokenLogin(client: ClientInterface) {
    const returnToken = sign({ _id: client._id }, `${envTokenClient}${client.password.password}`, {
      expiresIn: '1h',
    });

    await dbRedis.getClient().set(`token::client::${client._id}`, returnToken, 'ex', 3600);

    return returnToken;
  }

};

const clientTokenController = new ClientTokenController();
export default clientTokenController;
