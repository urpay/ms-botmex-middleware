import { UserInterface } from '../models/User';
import { Request, Response } from 'express';

import * as returnMessages from '../config/returnMessages';
import * as otplib from 'otplib';

import qrcode from 'qrcode';

import DbRedis from '../config/database/redis';
import { envAppName } from '../config/environment';
import ErrorLib from '../lib/ErrorLib';

class TwoFactorsController {

  public async getSecret(req: Request, res: Response) {

    const userFinded: UserInterface = res.locals.userFinded;

    if (userFinded.password.twoFactors.isActive) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eTwoFactorsIsActive,
        errorCode: 10,
      });
    }

    if (!userFinded.password.twoFactors.secret || req.query.reset) {
      userFinded.set('password.twoFactors.secret', otplib.authenticator.generateSecret());
      await userFinded.save();
    }

    res.json({
      secret: userFinded.password.twoFactors.secret,
      qrCode: await qrcode.toDataURL(otplib.authenticator.keyuri(userFinded.email.email, envAppName, userFinded.password.twoFactors.secret)),
    });
  }

  public setActive = async (req: Request, res: Response) => {

    const userFinded: UserInterface = res.locals.userFinded;

    if (userFinded.password.twoFactors.isActive) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eTwoFactorsIsActive,
        errorCode: 10,
      });
    }

    if (!userFinded.password.twoFactors.secret) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eTwoFactorsNoSecret,
        errorCode: 11,
      });
    }

    if (!otplib.authenticator.check(req.body.code2fa, userFinded.password.twoFactors.secret)) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eTwoFactorsInvalid,
        errorCode: 9,
        httpCode: 401,
      });
    }

    await this.setLastUsed(userFinded, req.body.code2fa);

    userFinded.set('password.twoFactors.isActive', true);
    await userFinded.save();

    res.json({ message: returnMessages.ptBr.twoFactorsActived });
  }

  public async setLastUsed(user: UserInterface, code: string) {
    await DbRedis.getClient().set(`last-two-factors::user::${user._id}`, code, 'ex', 30);
  }

  public async isLastUsed(user: UserInterface, code: string) {
    const lastUsed = await DbRedis.getClient().get(`last-two-factors::user::${user._id}`);

    if (lastUsed == code)
      return true;

    return false;
  }

}

export default new TwoFactorsController();
