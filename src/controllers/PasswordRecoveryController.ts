import genericLib from '../lib/GenericLib';
import dbRedis from '../config/database/redis';
import { User, UserInterface } from '../models/User';
import { Request, Response } from 'express';
import ErrorLib from '../lib/ErrorLib';
import * as returnMessages from '../config/returnMessages';
import { sendPasswordRecoveryEmail } from '../lib/Mail';


class PasswordRecoveryController {

  public checkCode = async (userFinded: UserInterface, code: string, type: string) => {
    const validCode = await dbRedis.getClient().get(`code::password::recovery-${type}::${userFinded._id}`);

    if (!validCode) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eCodeInvalid,
        errorCode: 14,
      });
    }

    if (validCode != code) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eCodeInvalid,
        errorCode: 14,
      });
    }

    return true;
  }

  public sendCodeRecoveryEmail = async (req: Request, res: Response) => {
    const userFinded = await User.findOne({ 'email.email': req.body.email }).exec();

    if (!userFinded) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eUserNotFound,
        errorCode: 20,
      });
    }

    //verifica se já existe um código gerado
    const codeExist = await dbRedis.getClient().get(`code::password::recovery-email::${userFinded._id}`);

    if (codeExist) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eCodeAlreadyExist,
        errorCode: 13,
      });
    }

    const code = genericLib.getRandomstring(6);

    dbRedis.getClient().set(`code::password::recovery-email::${userFinded._id}`, code, 'ex', 900);
    await sendPasswordRecoveryEmail(userFinded, code);

    res.json({
      message: returnMessages.ptBr.codeSent,
    });
  }

  public checkCodeEmail = async (req: Request, res: Response) => {
    const userFinded = await User.findOne({ 'email.email': req.body.email }).exec();

    if (!userFinded) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eUserNotFound,
        errorCode: 20,
      });
    }

    await this.checkCode(userFinded, req.body.code, 'email');
    res.json({ message: returnMessages.ptBr.eCodeValid });
  }

  public setNewPasswordEmail = async (req: Request, res: Response) => {
    const userFinded = await User.findOne({ 'email.email': req.body.email }).exec();

    if (!userFinded) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eUserNotFound,
        errorCode: 20,
      });
    }

    await this.checkCode(userFinded, req.body.code, 'email');

    if (!userFinded?.email?.isChecked) {
      userFinded.set('email.isChecked', true);
      await userFinded.validate();
      await userFinded.save();
    }

    await userFinded.updatePassword(req.body.password, req.body.passwordConfirm);
    await this.resetCodeAll(userFinded);

    res.json({ message: returnMessages.ptBr.passwordChanged });
  }



  public resetCodeAll = async (userFinded: UserInterface) => {
    await dbRedis.getClient().set(`code::password::recovery-email::${userFinded._id}`, '', 'ex', 1);
  }

}

const passwordRecoveryController = new PasswordRecoveryController();
export default passwordRecoveryController;
