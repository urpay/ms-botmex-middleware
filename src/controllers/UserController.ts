import { User, UserInterface } from '../models/User';
import passwordLib from '../lib/PasswordLib';
import { Request, Response } from 'express';

import * as returnMessages from '../config/returnMessages';

import * as Yup from 'yup';
import emailController from './EmailController';
import tokenController from './TokenController';

// @ts-ignore
import ErrorLib from '../lib/ErrorLib';

class UserController {

  public async store(req: Request, res: Response) {
    const yupSchema = Yup.object().shape({
      name: Yup.string().required(),
      email: Yup.string().email().required(),
      password: Yup.string().required().min(8),
      passwordConfirm: Yup.string().required().min(8),
    });

    if (!await yupSchema.isValid(req.body)) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eDataNotSent,
        errorCode: 0,
      });
    }

    const checkCounts = await Promise.all([
      User.countUsers({ 'email.email': req.body.email }),
      User.countUsers({ 'email.emailChange': req.body.email }),
    ]);

    const duplicateError = [];



    if (checkCounts[0] > 0)
      duplicateError.push(returnMessages.ptBr.eEmaiExist);

    if (checkCounts[1] > 0)
      duplicateError.push(returnMessages.ptBr.eEmaiExistChange);

    if (duplicateError.length > 0) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eInValidation,
        errors: duplicateError,
        errorCode: 1,
      });
    }

    const user = new User({
      name: req.body.name,
      email: {
        email: req.body.email,
      },
      password: {
        password: req.body.password,
      },
    });

    try {
      await user.validate();
    } catch (e) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eInValidation,
        errors: User.getMessageErrorArray(e),
        errorCode: 2,
      });
    }

    if (!passwordLib.check(user.password.password, req.body.passwordConfirm)) {
      throw new ErrorLib({
        message: returnMessages.ptBr.ePasswordInvalid,
        errorCode: 3,
      });
    }

    user.set('password.password', passwordLib.hash(user.password.password));

    try {
      await user.validate();
    } catch (e) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eInValidation,
        errors: User.getMessageErrorArray(e),
        errorCode: 2,
      });
    }

    await user.save();

    //envia o email de validação
    await emailController.sendValidation(user);

    const token = await tokenController.generateTokenLogin(user);

    res.json({
      user: user.getReturnJson(),
      token: token,
      message: returnMessages.ptBr.registerComplete,
    });
  }

  public async show(req: Request, res: Response) {
    const userFinded: UserInterface = res.locals.userFinded;
    res.json({ user: userFinded.getReturnJson() });
  }

  public async updatePassword(req: Request, res: Response) {
   

    const userFound: UserInterface = await User.findById(res.locals.userFinded).exec();

    if (!userFound) {
      res.status(422).send({ message: returnMessages.ptBr.eUserNotFound });
      return;
    }


    const user = await userFound.updatePassword(req.body.password, req.body.passwordConfirm);

    res.json({ user });

  }

}

export default new UserController();
