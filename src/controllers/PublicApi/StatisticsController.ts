import { Request, Response } from 'express';
import { Client } from '../../models/Client';
import { Wallet } from '../../models/Wallet';
import { Market } from '../../models/Market';
import { WalletTransaction } from '../../models/WalletTransaction';
import subMinutes from 'date-fns/subMinutes';
import { getMarketQuotation } from '../../jobs/GetMarketQuotation';
import { dailyLiquidation } from '../../jobs/SendTelegramMessages';

class StatisticController {
  public async getTotalClients(req: Request, res: Response): Promise<Response> {  
    const totalClients = await Client.countClients({});
    
    return res.status(200).json({ totalClients });
  }

  public async getBTCClientsSum(req: Request, res: Response): Promise<Response> {
    const sum = await Wallet.aggregate([{
      $group: {
        _id: null,
        totalBTC: {
          $sum: '$amount',
        },
      },
    }]);

    return res.status(200).send({ totalBTC: sum[0].totalBTC });
  }

  public async getMinAndMaxOperations(req: Request, res: Response): Promise<Response> {
    const lastWallet = await WalletTransaction.findOne({ transactType: 'RealisedPNL' })
      .sort({timestamp:-1})
      .exec();
    
    const minOperation = await WalletTransaction.findOne({
      transactType: 'RealisedPNL',
      timestamp: {
        $gt: subMinutes(lastWallet.timestamp, 40),
      } }).sort({ result: 1 });

    const maxOperation = await WalletTransaction.findOne({
      transactType: 'RealisedPNL',
      timestamp: {
        $gt: subMinutes(lastWallet.timestamp, 40),
      } }).sort({ result: -1 });

    return res.json({minOperation, maxOperation});
  }

  public async getMarketQuotation(req: Request, res: Response): Promise<Response> {
    await getMarketQuotation();

    const quotation = await Market.findOne({});

    return res.json({ quotation });
  }

  public async sucessRisk(req: Request, res: Response): Promise<Response> {
    const lastWallet = await WalletTransaction.findOne({ transactType: 'RealisedPNL' })
      .sort({timestamp: -1})
      .exec();

    const transactions = await WalletTransaction.find({
      transactType: 'RealisedPNL',
      timestamp: {
        $gt: subMinutes(lastWallet.timestamp, 40),
      },
    });

    let totalSuccess = 0;
    transactions.forEach((transaction) => {
      if (transaction.result > 0) {
        totalSuccess++;
      }
    });
    
    const totalSuccessRisk = (totalSuccess / transactions.length) * 100;

    return res.json({ totalSuccessRisk });
  }

  public async dailyLiquidation(req: Request, res: Response): Promise<Response> {
    dailyLiquidation();

    return res.send('Foi');
  }
}

export default new StatisticController();
