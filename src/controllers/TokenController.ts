import dbRedis from '../config/database/redis';
import { User, UserInterface } from '../models/User';

import { sign } from 'jsonwebtoken';
import { envTokenUser } from '../config/environment';
import { Request, Response } from 'express';

import * as returnMessages from '../config/returnMessages';

import * as Yup from 'yup';
import passwordLib from '../lib/PasswordLib';
import ErrorLib from '../lib/ErrorLib';

class TokenController {

  public generateTokenLoginRoute = async (req: Request, res: Response) => {
    const yupSchema = Yup.object().shape({
      email: Yup.string().required(),
      password: Yup.string().required(),
    });

    if (!await yupSchema.isValid(req.body)) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eDataNotSent,
        errorCode: 0,
      });
    }

    const finded = await User.findOne({ 'email.email': req.body.email }, '+password.password').exec();


    if (!finded) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eDataNotSent,
        errorCode: 4,
      });
    }

    if (!passwordLib.checkHash(req.body.password, finded.password.password)) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eDataNotSent,
        errorCode: 4,
      });
    }


    const returnToken = await this.generateTokenLogin(finded);

    res.json({
      user: finded.getReturnJson(),
      token: returnToken,
    });
  }

  public async generateTokenLogin(user: UserInterface) {
    const returnToken = sign({ _id: user._id }, `${envTokenUser}${user.password.password}`, {
      expiresIn: '1h',
    });

    await dbRedis.getClient().set(`token::user::${user._id}`, returnToken, 'ex', 3600);

    return returnToken;
  }

};

const tokenController = new TokenController();
export default tokenController;
