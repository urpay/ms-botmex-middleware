import { Client, ClientInterface } from '../models/Client';

import { Request, Response } from 'express';

import * as returnMessages from '../config/returnMessages';
import bitMexLib from '../lib/BitMexLib';

class MarginController {

  public async userMargin(req: Request, res: Response) {
    if (!req.params.clientId) {
      res.status(422).send({ message: returnMessages.ptBr.eDataNotSent });
      return;
    }

    const clientFound: ClientInterface = await Client.findById(req.params.clientId).exec();

    if (!clientFound) {
      res.status(422).send({ message: returnMessages.ptBr.eClientNotFound });
      return;
    }

    const margin = await bitMexLib.getUserMargin(clientFound);
    const orders = await bitMexLib.getOrder(clientFound);

    res.json({
      message: 'Sucesso ao buscar a margem do usuário',
      margin,
      orders,
    });
    
  }

};

const marginController = new MarginController();
export default marginController;
