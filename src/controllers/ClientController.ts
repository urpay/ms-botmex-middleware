import { Client, ClientInterface } from '../models/Client';

import { Request, Response } from 'express';

import * as returnMessages from '../config/returnMessages';

import * as Yup from 'yup';
import ErrorLib from '../lib/ErrorLib';
import passwordLib from '../lib/PasswordLib';
import clientService from '../services/ClientService';

class ClientController {

  public async store(req: Request, res: Response) {
    const yupSchema = Yup.object().shape({
      name: Yup.string().required(),
      email: Yup.string().email().required(),
      apiKey: Yup.string().required(),
      apiSecret: Yup.string().required(),
      password: Yup.string().required().min(8),
      passwordConfirm: Yup.string().required().min(8),
    });

    if (!await yupSchema.isValid(req.body)) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eDataNotSent,
        errorCode: 0,
      });
    }

    const checkCounts = await Promise.all([
      Client.countClients({ 'email.email': req.body.email }),
    ]);

    const duplicateError = [];



    if (checkCounts[0] > 0)
      duplicateError.push(returnMessages.ptBr.eEmaiExist);


    if (duplicateError.length > 0) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eInValidation,
        errors: duplicateError,
        errorCode: 1,
      });
    }



    const client = {
      name: req.body.name,
      email: {
        email: req.body.email,
      },
      password: {
        password: req.body.password,
      },
      initDate: req.body.initDate,
      apiIp: req.body.apiIp,
      apiKey: req.body.apiKey,
      apiSecret: req.body.apiSecret,
      operationRate: req.body.operationRate,
    };


    if (!passwordLib.check(client.password.password, req.body.passwordConfirm)) {
      throw new ErrorLib({
        message: returnMessages.ptBr.ePasswordInvalid,
        errorCode: 3,
      });
    }

    client.password.password = passwordLib.hash(client.password.password);


    try {
      const newClient = await clientService.create(client);
      res.json({
        user: newClient.getReturnJson(),
        message: returnMessages.ptBr.registerComplete,
      });
    } catch (e) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eInValidation,
        errors: Client.getMessageErrorArray(e),
        errorCode: 2,
      });
    }

  }


  public async show(req: Request, res: Response) {

    if (!req.params.clientId) {
      res.status(422).send({ message: returnMessages.ptBr.eDataNotSent });
      return;
    }

    const clientFound: ClientInterface = await Client.findById(req.params.clientId).exec();

    if (!clientFound) {
      res.status(422).send({ message: returnMessages.ptBr.eClientNotFound });
      return;
    }

    res.json({ client: clientFound.getReturnJson() });
  }


  public async list(req: Request, res: Response) {
    const clients = await clientService.find(req.body);
    res.json({ clients: clients });
  }

  public async update(req: Request, res: Response) {
    if (!req.params.clientId) {
      res.status(422).send({ message: returnMessages.ptBr.eDataNotSent });
      return;
    }

    const clientFound: ClientInterface = await Client.findById(req.params.clientId).exec();

    if (!clientFound) {
      res.status(422).send({ message: returnMessages.ptBr.eClientNotFound });
      return;
    }

    const client = {
      name: req.body.name,
      email: {
        email: req.body.email,
      },
      apiIp: req.body.apiIp,
      apiKey: req.body.apiKey,
      apiSecret: req.body.apiSecret,
      operationRate: req.body.operationRate,
    };

    await clientService.update(clientFound._id, client);

    res.json({ client });

  }

  public async updatePassword(req: Request, res: Response) {
    if (!req.params.clientId) {
      res.status(422).send({ message: returnMessages.ptBr.eDataNotSent });
      return;
    }

    const clientFound: ClientInterface = await Client.findById(req.params.clientId).exec();

    if (!clientFound) {
      res.status(422).send({ message: returnMessages.ptBr.eClientNotFound });
      return;
    }


    const client = await clientFound.updatePassword(req.body.password, req.body.passwordConfirm);

    res.json({ client });

  }

  public async updateApiStatus(req: Request, res: Response){
    let clientFound;

    try {
      clientFound = await Client.findById(req.params.clientId).exec();
    } catch (error) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eUserNotFound,
      });
    }

    if (!clientFound) {
      res.status(422).send({ message: returnMessages.ptBr.eUserNotFound });
      return;
    }

    clientFound.apiIsActive = !clientFound.apiIsActive;

    try {
      clientFound.save();   
    } catch (error) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eDataBase,
      });
    }

    res.status(200).json({message: returnMessages.ptBr.dataUpdated, clientFound});
  }
};

const clientController = new ClientController();
export default clientController;
