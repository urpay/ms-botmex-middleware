import { Request, Response } from 'express';
import { TradeHistory } from '../models/TradeHistory';

class TradeHistoryController {
  public async index(req: Request, res: Response): Promise<Response> {
    const { clientId } = req.params;

    const tradeHistory = await TradeHistory.find({ clientId }).sort({ transactTime: -1 });

    return res.status(200).json({ tradeHistory });
  }
}

export default new TradeHistoryController();
