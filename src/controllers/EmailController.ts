import { User, UserInterface } from '../models/User';
import { Request, Response } from 'express';

import * as returnMessages from '../config/returnMessages';
import dbRedis from '../config/database/redis';
import genericLib from '../lib/GenericLib';
import ErrorLib from '../lib/ErrorLib';
import { sendValidationEmail } from '../lib/Mail';

import { differenceInSeconds } from 'date-fns';

class EmailController {

  public sendValidation = async (user: UserInterface, force = false) => {

    //verifica se o email dele ja nao esta valido ou se ele tem um email pendente para troca
    if (user.email.isChecked && !user.email.emailChange) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eEmailIsChecked,
        errorCode: 12,
      });
    }

    const timeToResendEmail = 120;

    //consulta se foi enviado um email de validação para ele
    const isSended = await dbRedis.getClient().get(`emailsend::validation::user::${user._id}`);
    const dSeconds = differenceInSeconds(new Date(), new Date(isSended));
    if (dSeconds < timeToResendEmail && !force) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eEmailIsSent(timeToResendEmail - dSeconds),
        errorCode: 13,
        extraData: {
          seconds: timeToResendEmail - dSeconds,
        },
      });
    }

    //gera string aleatoria
    const code = genericLib.getRandomstring(6);

    //seta no redis que o email de validação foi enviado juntamente com o codigo
    await Promise.all([
      dbRedis.getClient().set(`emailsend::validation::user::${user._id}`, new Date(), 'ex', timeToResendEmail),
      dbRedis.getClient().set(`emailsend::validation::user::${user._id}::code`, code, 'ex', 10800),
    ]);

    //envia o email de validação
    await sendValidationEmail(user, code);

    return returnMessages.ptBr.emailSent;
  }

  public sendValidationRoute = async (req: Request, res: Response) => {
    const userFinded: UserInterface = res.locals.userFinded;
    res.json({ message: await this.sendValidation(userFinded) });
  }

  public async checkValidation(user: UserInterface, code: string) {

    //verifica se o email dele ja nao esta valido ou se ele tem um email pendente para troca
    if (user.email.isChecked && !user.email.emailChange) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eEmailIsChecked,
        errorCode: 12,
      });
    }

    //verifica se existe um codigo válido salvo no redis
    const codeExist = await dbRedis.getClient().get(`emailsend::validation::user::${user._id}::code`);

    if (codeExist != code) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eCodeInvalid,
        errorCode: 14,
      });
    }

    //caso exista um pendente para troca, faz a troca
    if (user.email.emailChange) {
      user.set('email.email', user.email.emailChange);
      user.set('email.emailChange', undefined);
    }

    user.set('email.isChecked', true);
    await user.validate();
    await user.save();

    //reseta no redis que o email de validação foi enviado juntamente com o codigo
    await Promise.all([
      dbRedis.getClient().set(`emailsend::validation::user::${user._id}`, true, 'ex', 1),
      dbRedis.getClient().set(`emailsend::validation::user::${user._id}::code`, '', 'ex', 1),
    ]);

    return returnMessages.ptBr.emailChecked;
  }

  public checkValidationRoute = async (req: Request, res: Response) => {
    const userFinded: UserInterface = res.locals.userFinded;
    res.json({ message: await this.checkValidation(userFinded, req.body.code) });
  }

  public changeEmail = async (user: UserInterface, email: string) => {

    if (user.email.email == email)
      throw new ErrorLib({
        message: returnMessages.ptBr.eEmaiUsed,
        errorCode: 15,
      });

    const checkCounts = await Promise.all([
      User.countUsers({ 'email.email': email }),
      User.countUsers({ 'email.emailChange': email }),
    ]);

    const duplicateError = [];

    if (checkCounts[0] > 0)
      duplicateError.push(returnMessages.ptBr.eEmaiExist);

    if (checkCounts[1] > 0)
      duplicateError.push(returnMessages.ptBr.eEmaiExistChange);

    if (duplicateError.length > 0) {
      throw new ErrorLib({
        message: returnMessages.ptBr.eInValidation,
        errors: duplicateError,
        errorCode: 1,
      });
    }

    if (user.email.isChecked) {
      user.set('email.emailChange', email);
    } else {
      user.set('email.email', email);
    }

    await user.validate();
    await user.save();
    await this.sendValidation(user, true);

    return returnMessages.ptBr.dataUpdated;
  }

  public changeEmailRoute = async (req: Request, res: Response) => {
    const userFinded: UserInterface = res.locals.userFinded;
    res.json({ message: await this.changeEmail(userFinded, req.body.email) });
  }

}

const emailController = new EmailController();
export default emailController;
