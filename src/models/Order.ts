import { Schema, model, Document, Model, Types } from 'mongoose';

import _ from 'lodash';


export interface OrderInterface extends Document {
  clientId: Types.ObjectId;
 
  createdAt: Date;
  updatedAt: Date;
}

export interface OrderModel extends Model<OrderInterface> {
  countOrders(query: any): Promise<number>;
  getMessageErrorArray(error:any): string[];
}

const OrderSchema = new Schema({
  clientId: {
    type: Types.ObjectId,
    required: true,
    ref: 'Client',
    index: true,
    unique:true,
  },
}, { timestamps: true });



OrderSchema.statics.countOrders = async function (query: any = {}) {
  const countOrders = await this.find(query).select('_id').lean().countDocuments().exec();
  return countOrders;
};





OrderSchema.statics.getMessageErrorArray = function (error: any) {
  const ErrorsArray: string[] = [];
  _.forIn(error.errors, async (e) => {
    if (e.name == 'ValidatorError')
      ErrorsArray.push(e.message);
  });
  return ErrorsArray;
};

export const Order: OrderModel = model<OrderInterface, OrderModel>('order', OrderSchema);
