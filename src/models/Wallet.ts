
import { Schema, model, Document, Model, Types } from 'mongoose';

export interface WalletInterface extends Document {
  clientId: Types.ObjectId;
  account: number;
  currency: string;
  prevDeposited: number;
  prevWithdrawn: number;
  prevTransferIn: number;
  prevTransferOut: number;
  prevAmount: number;
  prevTimestamp: Date;
  deltaDeposited: number;
  deltaWithdrawn: number;
  deltaTransferIn: number;
  deltaTransferOut: number;
  deltaAmount: number;
  deposited: number;
  withdrawn: number;
  transferIn: number;
  transferOut: number;
  amount: number;
  pendingCredit: number;
  pendingDebit: number;
  confirmedDebit: number;
  Date?: Date;
  addr: string;
  script: string;
}

export interface WalletModel extends Model<WalletInterface> {
  countWallets(query: any): Promise<number>;
}



const WalletSchema = new Schema({
  clientId: {
    type: Types.ObjectId,
    required: true,
    ref: 'Client',
    index: true,
    unique: true,
  },
  account: {
    type: Number,
    required: true,
  },
  currency: {
    type: String,
    required: true,
  },
  prevDeposited: {
    type: Number,
    required: true,
  },
  prevWithdrawn: {
    type: Number,
    required: true,
  },
  prevTransferIn: {
    type: Number,
    required: true,
  },
  prevTransferOut: {
    type: Number,
    required: true,
  },
  prevAmount: {
    type: Number,
    required: true,
  },
  prevTimestamp: {
    type: Date,
    required: true,
  },
  deltaDeposited: {
    type: Number,
    required: true,
  },
  deltaWithdrawn: {
    type: Number,
    required: true,
  },
  deltaTransferIn: {
    type: Number,
    required: true,
  },
  deltaTransferOut: {
    type: Number,
    required: true,
  },
  deltaAmount: {
    type: Number,
    required: true,
  },
  deposited: {
    type: Number,
    required: true,
  },
  withdrawn: {
    type: Number,
    required: true,
  },
  transferIn: {
    type: Number,
    required: true,
  },
  transferOut: {
    type: Number,
    required: true,
  },
  amount: {
    type: Number,
    required: true,
  },
  pendingCredit: {
    type: Number,
    required: true,
  },
  pendingDebit: {
    type: Number,
    required: true,
  },
  confirmedDebit: {
    type: Number,
    required: true,
  },
  Date: {
    type: Date,
  },
  addr: {
    type: String,
    required: true,
  },
  script: {
    type: String,
    required: true,
  },
}, { timestamps: true });



WalletSchema.statics.countWallets = async function (query: any = {}) {
  const countedWallets = await this.find(query).select('_id').lean().countDocuments().exec();
  return countedWallets;
};

export const Wallet: WalletModel = model<WalletInterface, WalletModel>('Wallets', WalletSchema);
