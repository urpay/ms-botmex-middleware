
import { Schema, model, Document, Model, Types } from 'mongoose';

export interface RealisedTransaction {
  transactID: string;
  transactTime: Date;
  amount: number;
  rate: number;
}

export interface PaymentInterface extends Document {
  clientId: Types.ObjectId;
  amount?: number;
  rate?: number;
  billingDate?: Date;
  dueDate?: Date;
  status?: string;
  walletAddress: string;
  walletDescription: string;
  walletSource: string;
  realised: RealisedTransaction;
  walletTransaction: Types.ObjectId;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface PaymentModel extends Model<PaymentInterface> {
  countPayment(query: any): Promise<number>;
}



const PaymentSchema = new Schema({
  clientId: {
    type: Types.ObjectId,
    required: true,
    ref: 'Client',
    index: true,
  },
  amount: {
    type: Number,
    required: true,
    default: 0,
  },
  rate: {
    type: Number,
    required: true,
    default: 0,
  },
  billingDate: {
    type: Date,
    required: true,
    index: true,
  },
  dueDate: {
    type: Date,
    required: true,
    index: true,
  },
  status: {
    type: String,
    required: true,
    index: true,
    enum: ['pending', 'processed'],
    default: 'pending',
  },
  walletAddress: {
    type: String,
    required: false,
    unique:true,
  },
  walletDescription: {
    type: String,
    required: false,
  },
  walletSource: {
    type: String,
    required: false,
  },
  realised:
    new Schema({
      transactID: {
        type: String,
        required: true,
        index:true,
        unique:true,
      },
      transactTime: {
        type: String,
        required: true,
        index: true,
      },
      amount: {
        type: Number,
        required: true,
      },
      rate: {
        type: Number,
        required: true,
      },
    }, { timestamps: true }),
  walletTransaction:{
    type: Types.ObjectId,
    ref: 'WalletTransaction',
    required: true,
    unique:true,
  },

}, { timestamps: true });



PaymentSchema.statics.countPayment = async function (query: any = {}) {
  const countPayment = await this.find(query).select('_id').lean().countDocuments().exec();
  return countPayment;
};

export const Payment: PaymentModel = model<PaymentInterface, PaymentModel>('Payments', PaymentSchema);
