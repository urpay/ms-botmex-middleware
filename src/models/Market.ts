
import { Schema, model, Document } from 'mongoose';

export interface MarketInterface extends Document {
  currency: string,
  currencyValue: number,
  marketStatus: string,
}

const MarketSchema = new Schema({
  currency: {
    type: String,
    required: [true, 'É obrigatório informar a moeda.'],
  },
  currencyValue: {
    type: Number,
    required: [true, 'É obrigatório informar o valor da moeda.'],
  },
  marketStatus: {
    type: String,
  },
}, { timestamps: true });


export const Market = model<MarketInterface>('Market', MarketSchema);
