import { Schema, model, Document, HookNextFunction, Model } from 'mongoose';
import { isEmail } from 'validator';

import passwordLib from '../lib/PasswordLib';
import ErrorLib from '../lib/ErrorLib';
import * as returnMessages from '../config/returnMessages';

import _ from 'lodash';



export interface UserInterface extends Document {
  name: string;
  email: {
    email: string;
    emailChange?: string;
    isChecked?: boolean;
    createdAt?: Date;
    updatedAt?: Date;
  };
  password: {
    password: string;
    twoFactors?: {
      type?: string;
      isActive?: boolean;
      secret?: string;
      createdAt?: Date;
      updatedAt?: Date;
    };
    createdAt?: Date;
    updatedAt?: Date;
  };
  createdAt?: Date;
  updatedAt?: Date;

  getReturnJson(): any;
  updatePassword(password: string, passwordConfirm: string): Promise<UserInterface>;
}

export interface UserModel extends Model<UserInterface> {
  countUsers(query: any): Promise<number>;
  getMessageErrorArray(error: any): string[];
}

const UserSchema = new Schema({
  name: {
    type: String,
    required: true,
    maxlength: 220,
  },

  email: new Schema({
    email: {
      type: String,
      lowercase: true,
      maxlength: 220,
      required: true,
      unique: true,
      sparse: true,
      validate: {
        validator: isEmail,
      },
    },
    emailChange: {
      type: String,
      lowercase: true,
      maxlength: 220,
      required: false,
      validate: {
        validator: isEmail,
      },
    },
    isChecked: {
      type: Boolean,
      default: false,
      index: true,
    },
  }, { _id: false, timestamps: true }),



  password: new Schema({
    password: {
      type: String,
      required: true,
      select: false,
    },
    twoFactors: new Schema({
      type: {
        type: String,
        enum: ['otp'],
      },
      isActive: {
        type: Boolean,
        default: false,
        index: true,
      },
      secret: {
        type: String,
        select: false,
      },
    }, { _id: false, timestamps: true }),

  }, { _id: false, timestamps: true }),

}, { timestamps: true });

UserSchema.pre('validate', function (next: HookNextFunction) {
  if (this.isNew) {
    this.set('password.twoFactors.isActive', false);
  }
  next();
});

UserSchema.methods.getReturnJson = function () {
  const thisUser: UserInterface = this;
  const returnJson = thisUser.toJSON();
  delete returnJson.password.password;
  delete returnJson.password.twoFactors.secret;

  if (returnJson.email) {
    if (returnJson.email.email)
      returnJson.email.email = `${returnJson.email.email.substr(0, 3)}****${returnJson.email.email.substr(returnJson.email.email.indexOf('@'))}`;

    if (returnJson.email.emailChange)
      returnJson.email.emailChange = `${returnJson.email.emailChange.substr(0, 3)}****${returnJson.email.emailChange.substr(returnJson.email.emailChange.indexOf('@'))}`;
  }
  return returnJson;
};

UserSchema.methods.updatePassword = async function (password: string, passwordConfirm: string) {
  const thisUser: UserInterface = this;

  if (!passwordLib.check(password, passwordConfirm)) {
    throw new ErrorLib({
      message: returnMessages.ptBr.ePasswordInvalid,
      errorCode: 3,
    });
  }

  thisUser.set('password.password', passwordLib.hash(password));
  await thisUser.save();

  return thisUser;
};

UserSchema.statics.countUsers = async function (query: any = {}) {
  const thisModel: UserModel = this;
  const UserCounted = await thisModel.find(query).select('_id').lean().countDocuments().exec();
  return UserCounted;
};

UserSchema.statics.getMessageErrorArray = function (error: any) {
  const ErrorsArray: string[] = [];
  _.forIn(error.errors, async (e) => {
    if (e.name == 'ValidatorError')
      ErrorsArray.push(e.message);
  });
  return ErrorsArray;
};

export const User: UserModel = model<UserInterface, UserModel>('User', UserSchema);
