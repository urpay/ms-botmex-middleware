
import { Schema, model, Document, Model, Types } from 'mongoose';
import { Client } from './Client';

export interface WalletTransactionInterface extends Document {
  clientId: Types.ObjectId;
  transactID: string;
  account: number;
  currency: string;
  transactType: string;
  amount: number;
  fee?: number;
  transactStatus?: string;
  address?: string;
  tx?: string;
  text?: string;
  transactTime: Date;
  walletBalance: number;
  marginBalance?: number;
  result: number;
  rate?: {
    rate: number;
    value: number;
  }
  timestamp?: Date;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface WalletTransactionModel extends Model<WalletTransactionInterface> {
  countWalletTransactions(query: any): Promise<number>;
}



const WalletTransactionSchema = new Schema({
  clientId: {
    type: Types.ObjectId,
    required: true,
    ref: 'Client',
    index: true,
  },

  transactID: {
    type: String,
    required: true,
    index:true,
    unique:true,
  },
  account: {
    type: Number,
    required: true,
  },
  currency: {
    type: String,
    required: true,
  },
  transactType: {
    type: String,
    required: true,
    index:true,
  },
  amount: {
    type: Number,
    required: true,
  },
  fee: {
    type: Number,
  },
  transactStatus: {
    type: String,
    required: true,
  },
  address: {
    type: String,
  },
  tx: {
    type: String,
  },
  text: {
    type: String,
  },
  transactTime: {
    type: Date,
    required:true,
    index:true,
  },
  walletBalance: {
    type: Number,
    required: true,
  },
  marginBalance: {
    type: Number,
  },
  result: {
    type: Number,
    default: 0,
    required: true,
  },
  rate: new Schema({
    rate: {
      type: Number,
      required: true,
      default: 0,
    },
    value: {
      type: Number,
      required: true,
      default: 0,
    },
  }, { _id: false }),
  timestamp: {
    type: Date,
    required: true,
    index:true,
  },


}, { timestamps: true });



WalletTransactionSchema.pre('save', async function (next) {
  const walletTransaction: WalletTransactionInterface = this.toJSON();

  const client = await Client.findById(walletTransaction.clientId).exec();
  if (walletTransaction.transactType == 'RealisedPNL') {
    this.set('result', walletTransaction.amount / walletTransaction.walletBalance * 100);
    this.set('rate', { rate: client?.operationRate, value: walletTransaction.amount * client?.operationRate/100 });
  }
  
});

WalletTransactionSchema.statics.countWalletTransactions = async function (query: any = {}) {
  const countedWalletTransactions = await this.find(query).select('_id').lean().countDocuments().exec();
  return countedWalletTransactions;
};

export const WalletTransaction: WalletTransactionModel = model<WalletTransactionInterface, WalletTransactionModel>('WalletTransaction', WalletTransactionSchema);
