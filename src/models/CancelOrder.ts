import { Schema, model, Document, Model, Types } from 'mongoose';

import _ from 'lodash';


export interface CancelOrderInterface extends Document {
  clientId: Types.ObjectId;
  stopLoss?: number;
  isActive: boolean;
  createdAt: Date;
  updatedAt: Date;
}

export interface CancelOrderModel extends Model<CancelOrderInterface> {
  countOrders(query: any): Promise<number>;
  getMessageErrorArray(error:any): string[];
}

const CancelOrderSchema = new Schema({
  clientId: {
    type: Types.ObjectId,
    required: true,
    ref: 'Client',
    index: true,
    unique:true,
  },
  stopLoss: {
    type: Number,
  },
  isActive:{
    type: Boolean,
    required:true,
    default:false,
  },

}, { timestamps: true });



CancelOrderSchema.statics.countOrders = async function (query: any = {}) {
  const countOrders = await this.find(query).select('_id').lean().countDocuments().exec();
  return countOrders;
};





CancelOrderSchema.statics.getMessageErrorArray = function (error: any) {
  const ErrorsArray: string[] = [];
  _.forIn(error.errors, async (e) => {
    if (e.name == 'ValidatorError')
      ErrorsArray.push(e.message);
  });
  return ErrorsArray;
};

export const CancelOrder: CancelOrderModel = model<CancelOrderInterface, CancelOrderModel>('cancelOrder', CancelOrderSchema);
