import { Schema, model, Document, Model } from 'mongoose';
import passwordLib from '../lib/PasswordLib';
import * as returnMessages from '../config/returnMessages';
import ErrorLib from '../lib/ErrorLib';
import { isEmail } from 'validator';
import _ from 'lodash';
import { toDate, startOfToday } from 'date-fns';

export interface ClientInterface extends Document {
  email: {
    email: string;
    emailChange?: string;
    isChecked?: boolean;
    createdAt?: Date;
    updatedAt?: Date;
  };
  name: string;
  telegramId: string;
  operationRate: number;
  initPaymentDate?:Date;
  apiIp?: string;
  apiKey: string;
  apiSecret: string;
  apiIsActive: boolean;
  lastResult: number;
  isActive:boolean;
  password: {
    password: string;
    twoFactors?: {
      type?: string;
      isActive?: boolean;
      secret?: string;
      createdAt?: Date;
      updatedAt?: Date;
    };
    createdAt?: Date;
    updatedAt?: Date;
  };  
  createdAt?: Date;
  updatedAt?: Date;
  getReturnJson(): any;
  updatePassword(password: string, passwordConfirm: string): Promise<ClientInterface>;
}

export interface ClientModel extends Model<ClientInterface> {
  countClients(query: any): Promise<number>;
  getMessageErrorArray(error: any): string[];
}

const ClientSchema = new Schema({
  email: new Schema({
    email: {
      type: String,
      lowercase: true,
      maxlength: 220,
      required: true,
      unique: true,
      sparse: true,
      validate: {
        validator: isEmail,
      },
    },
    emailChange: {
      type: String,
      lowercase: true,
      maxlength: 220,
      required: false,
      validate: {
        validator: isEmail,
      },
    },
    isChecked: {
      type: Boolean,
      default: false,
      index: true,
    },
  }, { _id: false, timestamps: true }),
  telegramId: { type: String, default: '' },
  operationRate: {
    type: Number,
    required: true,
    default: 30,
  },
  name: {
    type: String,
    required: true,
  },
  apiIp: {
    type: String,
  },
  apiKey: {
    type: String,
    required: true,
    unique: true,
  },
  apiIsActive: {
    type: Boolean,
    default: true,
  },
  initPaymentDate:{
    type: Date,
    default: toDate(startOfToday()),
    required: true,
  },
  nextPaymentDate:{
    type: Date,
  },
  isActive:{
    type: Boolean,
    required: true,
    default: true,
  },
  lastPaymentDate:{
    type: Date,
  },
  apiSecret: {
    type: String,
    required: true,
  },
  lastResult:{
    type: Number,
  },
  password: new Schema({
    password: {
      type: String,
      required: true,
      select: false,
    },
    twoFactors: new Schema({
      type: {
        type: String,
        enum: ['otp'],
      },
      isActive: {
        type: Boolean,
        default: false,
        index: true,
      },
      secret: {
        type: String,
        select: false,
      },
    }, { _id: false, timestamps: true }),
    
  }, { _id: false, timestamps: true }),

}, { timestamps: true });



ClientSchema.statics.countClients = async function (query: any = {}) {
  const clientCounted = await this.find(query).select('_id').lean().countDocuments().exec();
  return clientCounted;
};


ClientSchema.methods.getReturnJson = function () {
  const thisClient: ClientInterface = this;
  const returnJson = thisClient.toJSON();
  delete returnJson.password.password;

  return returnJson;
};


ClientSchema.methods.updatePassword = async function (password: string, passwordConfirm: string) {
  const thisClient: ClientInterface = this;
  
  if (!passwordLib.check(password, passwordConfirm)) {
    throw new ErrorLib({
      message: returnMessages.ptBr.ePasswordInvalid,
      errorCode: 3,
    });
  }
  
  thisClient.set('password.password', passwordLib.hash(password));
  await thisClient.save();
  
  return thisClient;
};


ClientSchema.statics.getMessageErrorArray = function (error: any) {
  const ErrorsArray: string[] = [];
  _.forIn(error.errors, async (e) => {
    if (e.name == 'ValidatorError')
      ErrorsArray.push(e.message);
  });
  return ErrorsArray;
};

export const Client: ClientModel = model<ClientInterface, ClientModel>('Client', ClientSchema);
