import { Payment, PaymentInterface } from '../models/Payment';
import AbstractModelService from './AbstractModelService';

class PaymentService extends AbstractModelService<PaymentInterface> {

  public constructor() {
    super(Payment);
  }

}

const paymentService = new PaymentService();
export default paymentService;