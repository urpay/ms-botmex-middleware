/* eslint-disable @typescript-eslint/no-parameter-properties */
import { Document, Model, QueryFindOneAndUpdateOptions, Types } from 'mongoose';

interface FindOpitonsInterface {
  params: { [key: string]: any };
  fields?: string;
  page?: number;
  pageSize?: number;
  sort?: { [key: string]: any };
  populate?: string,
}
/**
 * Classe abstrata para os serviços.
 */
export default abstract class AbstractModelService<T extends Document> {
  // abstract isValid(data: any): boolean;

  public constructor(protected model: Model<T>) { }

  public validateId = (id: any) => {
    if (!Types.ObjectId.isValid(id)) {
      throw new Error('Object not found.');
    }
    return id;
  };

  public async create(data: any) {
    const instance = new this.model(data);
    await instance.validate();
    await instance.save();

    return instance;
  }

  public async find(options: FindOpitonsInterface) {
    const page = options.page && options.page > 0 ? options.page : 1;
    const pageSize =
      options.pageSize && options.pageSize > 0 && options.pageSize < 500
        ? options.pageSize
        : 500;
    const skip = (page - 1) * pageSize;

    const total = await this.model.countDocuments(options.params);
    const fields = options.fields || '';
    const sort = options.sort || '';
    const populate = options.populate || '';

    const documents = await this.model
      .find(options.params)
      .sort(sort)
      .skip(skip)
      .limit(pageSize)
      .select(fields)
      .populate(populate)
      .exec();

    return { total, page, pageSize, documents };
  }

  public async findById(id: any) {
    return await this.model.findById(this.validateId(id)).exec();
  }

  public async update(id: any, data: any) {
    const options: QueryFindOneAndUpdateOptions = {
      runValidators: true,
      new: true,
    };
    const updatedDocument = await this.model.findByIdAndUpdate(
      id,
      data,
      options,
    );

    return updatedDocument;
  }


}

