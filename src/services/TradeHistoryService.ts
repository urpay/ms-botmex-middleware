import AbstractModelService from './AbstractModelService';
import { TradeHistoryInterface, TradeHistory } from '../models/TradeHistory';

class TradeService extends AbstractModelService<TradeHistoryInterface> {
  public constructor() {
    super(TradeHistory);
  }

  public async createOrUpdateTradeHistory(clientId: any, trades: any[]) {
    trades.forEach(async (trade: TradeHistoryInterface) => {
      let tradeExists = await TradeHistory.findOne({ execID: trade.execID });
      
      if (!tradeExists) {
        try {
          await this.create({ clientId, ...trade });
          return;
        } catch (error) {
          console.log(error);
        }
      }
    });
  }
}

export default new TradeService();