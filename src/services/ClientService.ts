import { Client, ClientInterface } from '../models/Client';
import AbstractModelService from './AbstractModelService';

class ClientService extends AbstractModelService<ClientInterface> {

  public constructor() {
    super(Client);
  }

  public async create(data: any) {
    //Agenda Pagamentol
    return await super.create(data);
  }


}

const clientService = new ClientService();
export default clientService;
