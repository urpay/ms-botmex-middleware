import { WalletTransaction, WalletTransactionInterface } from '../models/WalletTransaction';
import AbstractModelService from './AbstractModelService';
import { Types } from 'mongoose';

class WalletTransactionService extends AbstractModelService<WalletTransactionInterface> {

  public constructor() {
    super(WalletTransaction);
  }

  public async createOrUpdate(walletTransaction: any) {

    const { transactID } = walletTransaction;
    const walletTransactionFound: WalletTransactionInterface = await WalletTransaction.findOne(
      {
        transactID,
      },
    ).exec();

    if (!walletTransactionFound) {
      return (await this.create({ ...walletTransaction })).toJSON();
    }

    const updatedDocument = await this.update(
      walletTransactionFound._id,
      { ...walletTransaction });

    return updatedDocument;
  }

  public async updateWalletTransactionList(clientId: Types.ObjectId, walletTransactionList: any[]): Promise<any> {
    const mapWallet = Promise.all(
      walletTransactionList.filter((transaction: any) => transaction.transactID != '00000000-0000-0000-0000-000000000000')
        .map(async (transaction: any) => {
          return await this.createOrUpdate({ clientId, ...transaction });
        }),
    );
    return mapWallet;
  }
}

const walletTransactionService = new WalletTransactionService();
export default walletTransactionService;
