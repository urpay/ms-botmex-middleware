import { Wallet, WalletInterface } from '../models/Wallet';
import AbstractModelService from './AbstractModelService';

class WalletService extends AbstractModelService<WalletInterface> {

  public constructor() {
    super(Wallet);
  }

  public async createOrUpdate(wallet: any) {    
    let clientWallet = await Wallet.findOne({ clientId: wallet.clientId });
  
    if (!clientWallet) {
      await Wallet.create(wallet);
      return;
    }

    await this.update(clientWallet._id, wallet);
  }
}

const walletService = new WalletService();
export default walletService;
