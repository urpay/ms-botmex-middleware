import { Market, MarketInterface } from '../models/Market';
import AbstractModelService from './AbstractModelService';

class MarketQuotationService extends AbstractModelService<MarketInterface> {

  public constructor() {
    super(Market);
  }

  public async createOrUpdate(currency: any) {
    const currencyName = 'USD';

    let foundCurrency = await Market.findOne({ currency: currencyName });

    if (currency['15m'] === foundCurrency?.currencyValue) {
      return;
    }

    if (!foundCurrency) {
      try {
        await Market.create({ currency: currencyName, currencyValue: currency['15m'], marketStatus: '' });
        return;
      } catch (error) {
        console.log(error);
      }
    }

    try {
      await this.update(foundCurrency._id, {
        currency: currencyName,
        currencyValue: currency['15m'],
        marketStatus: currency['15m'] >= foundCurrency.currencyValue ? 'alta' : 'baixa',
      });
    } catch (error) {
      console.log(error);
    }
  }
}

const marketQuotationService = new MarketQuotationService();
export default marketQuotationService;
