import { Request, Response, NextFunction } from 'express';
import { decode, verify } from 'jsonwebtoken';

import * as returnMessages from '../config/returnMessages';
import DbRedis from '../config/database/redis';
import { Client } from '../models/Client';
import {  envTokenClient } from '../config/environment';
import ErrorLib from '../lib/ErrorLib';

export default async (req: Request, res: Response, next: NextFunction) => {
  const authHeader = req.headers.authorization;

  if (!authHeader) {
    throw new ErrorLib({
      message: returnMessages.ptBr.eTokenNotSent,
      errorCode: 5,
      httpCode: 401,
    });
  }


  const [, token] = authHeader.split(' ');

  if (!token) {
    throw new ErrorLib({
      message: returnMessages.ptBr.eTokenNotSent,
      errorCode: 5,
      httpCode: 401,
    });
  }

  const tokenDecoded: any = decode(token);

  const redisToken = await DbRedis.getClient().get(`token::client::${tokenDecoded._id}`);

  if (!redisToken || redisToken != token) {
    throw new ErrorLib({
      message: returnMessages.ptBr.eTokenInvalid,
      errorCode: 6,
      httpCode: 401,
    });
  }

  const finded = await Client.findOne({ _id:tokenDecoded._id }, '+password.password +password.twoFactors.secret').exec();

  try {
    await verify(token, `${envTokenClient}${finded.password.password}`);
  } catch (e) {
    throw new ErrorLib({
      message: returnMessages.ptBr.eTokenInvalid,
      errorCode: 6,
      httpCode: 401,
    });
  }

  res.locals.tokenDecoded = tokenDecoded;
  res.locals.clientFound = finded;
  next();
};
