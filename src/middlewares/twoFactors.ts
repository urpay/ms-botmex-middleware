import { Request, Response, NextFunction } from 'express';
import * as returnMessages from '../config/returnMessages';
import { UserInterface } from '../models/User';
import twoFactorsController from '../controllers/TwoFactorsController';

import * as otplib from 'otplib';
import ErrorLib from '../lib/ErrorLib';

export default async (req: Request, res: Response, next: NextFunction) => {

  const userFinded: UserInterface = res.locals.userFinded;

  if (!userFinded.password.twoFactors.isActive) {
    throw new ErrorLib({
      message: returnMessages.ptBr.eTwoFactorsNoActive,
      errorCode: 7,
      httpCode: 401,
    });
  }

  const code2fa = req.body.code2fa || req.query.code2fa;

  if (await twoFactorsController.isLastUsed(userFinded, code2fa)) {
    throw new ErrorLib({
      message: returnMessages.ptBr.eTwoFactorsIsLastUsed,
      errorCode: 8,
      httpCode: 401,
    });
  }

  if (!otplib.authenticator.check(code2fa, userFinded.password.twoFactors.secret)) {
    throw new ErrorLib({
      message: returnMessages.ptBr.eTwoFactorsInvalid,
      errorCode: 9,
      httpCode: 401,
    });
  }

  await twoFactorsController.setLastUsed(userFinded, code2fa);

  next();
};
