import RateLimit from 'express-rate-limit';

// @ts-ignore
import RedisStore from 'rate-limit-redis';
import dbRedis from '../config/database/redis';

import * as returnMessages from '../config/returnMessages';

import { differenceInSeconds } from 'date-fns';

import ms from 'ms';
import ErrorLib from '../lib/ErrorLib';
import { envIsDev, envIsTest, envNotRate } from '../config/environment';

export default (prefix: string, maxRequest = 5, time = '1m', resetExpiryOnChange = true) => {
  maxRequest = envIsDev() || envIsTest() || envNotRate() ? 999 : maxRequest;
  return new RateLimit({
    store: new RedisStore({
      client: dbRedis.getClient(),
      prefix: `rl::${prefix}`,
      resetExpiryOnChange,
      expiry: ms(time) / 1000,
    }),
    max: maxRequest,
    handler: async function (req, res) {
      // @ts-ignore
      const timeToReset = req.rateLimit.resetTime;

      const rateError = new ErrorLib({
        message: returnMessages.ptBr.eRequestMaxReached,
        errorCode: 17,
        httpCode: 429,
        extraData: {
          secondsToReset: differenceInSeconds(new Date(timeToReset), new Date()),
        },
      });

      res.status(rateError.getHttpCode()).json(rateError.getErrorJson());
      return;
    },
  });
};
