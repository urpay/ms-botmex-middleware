import { envBlockchain } from '../config/environment';
import Axios, { AxiosInstance } from 'axios';

class BlockchainLib {
  protected apiAxios: AxiosInstance;

  public constructor() {
    this.apiAxios = Axios.create({
      baseURL: envBlockchain.apiUrl,
    });
  }

  public async getMarketQuotation() {
    const quotation = await this.apiAxios.get('/ticker');

    return quotation.data;
  }
}

const blockchainLib = new BlockchainLib();
export default blockchainLib;
