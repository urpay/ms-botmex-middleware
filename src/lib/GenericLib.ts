import randomstring from 'randomstring';

class GenericLib {

  public getRandomstring(length: number) {
    return randomstring.generate({ length, charset: 'ABCDEFGHJKMNPQRSTUVWXYZ123456789' });
  }

  public getRandomNumber(length: number) {
    return randomstring.generate({ length, charset: '0123456789' });
  }

};

const genericLib = new GenericLib();
export default genericLib;
