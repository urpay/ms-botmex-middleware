
import { UserInterface } from '../../../models/User';
import ServicesLib from 'npm-services-lib';
const servicesLib = new ServicesLib();

export const sendValidationEmail = async(user: UserInterface, code: string) =>{
  await servicesLib.sendToQueue({
    queue: 'send.mail.validation',
    messageBuffer: servicesLib.getBufferJson({
      user,
      code,
      request_date: new Date(),
    }),
  });
};