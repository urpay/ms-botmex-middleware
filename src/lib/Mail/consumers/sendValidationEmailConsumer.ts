
import ServicesLib from 'npm-services-lib';
import mailLib from '../MailLib';
const servicesLib = new ServicesLib();



servicesLib.consumeQueue('send.mail.validation', async (message) => {

  const msgJson= servicesLib.getJsonMessage(message);
  const { code, user } =msgJson;
  await mailLib.sendValidationEmail(user, code);
  
  servicesLib.aproveMessage(message);
});
