

import { resolve } from 'path';

import nodemailer, { Transporter } from 'nodemailer';
import exphbs from 'express-handlebars';

// @ts-ignore
import nodemailerhbs from 'nodemailer-express-handlebars';

// @ts-ignore
import sgTransport from 'nodemailer-transport-sendgrid';
import { SendMailOptions } from 'nodemailer';
import { UserInterface } from '../../models/User';
import { envSendgrid } from '../../config/environment';

class MailLib {

  private transporter: Transporter


  public constructor() {
    this.transporter = nodemailer.createTransport(sgTransport(envSendgrid.apiKey));

    this.configureTemplates();
  }

  private configureTemplates() {
    const viewPath = resolve(__dirname, './', 'emails');

    this.transporter.use('compile', nodemailerhbs({
      viewEngine: exphbs.create({
        layoutsDir: resolve(viewPath, 'layouts'),
        partialsDir: resolve(viewPath, 'partials'),
        defaultLayout: 'default',
        extname: '.hbs',
      }),
      viewPath,
      extName: '.hbs',
    }));
  }

  private sendMail(message: SendMailOptions) {
    return this.transporter.sendMail({
      from: envSendgrid.email,
      ...message,
    });
  }

  public async sendValidationEmail(user: UserInterface, link: string) {
    return await this.sendMail({
      to: user.email.email,
      subject: 'Validação de e-mail',
      // @ts-ignore
      template: 'confirmation',
      context: {
        userName: user.name,
        link,
      },
      category: envSendgrid.category,
    });
  }

  public async sendRecoveryPasswordEmail(user: UserInterface, link: string) {
    return await this.sendMail({
      to: user.email.email,
      subject: 'Validação de e-mail',
      // @ts-ignore
      template: 'recover-password-email',
      context: {
        userName: user.name,
        link,
      },
      category: envSendgrid.category,
    });
  }



};

const mailLib = new MailLib();
export default mailLib;