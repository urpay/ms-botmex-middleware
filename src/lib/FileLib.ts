import ServicesLib from 'npm-services-lib';
const servicesLib = new ServicesLib();

class FileLib {

  public async sendFile(fileName: string, fileBuffer: Buffer) {
    const response = await servicesLib.sendToQueue({
      queue: 'send.file.buffer',
      messageBuffer: fileBuffer,
      awaitResponse: true,
      extraData: JSON.stringify({ fileName }),
    });
    return servicesLib.getJsonMessage(response);
  }

  public async sendStaticFile(fileBuffer: Buffer, fileId: any = false) {
    const response = await servicesLib.sendToQueue({
      queue: 'send.file.static.buffer',
      messageBuffer: fileBuffer,
      awaitResponse: true,
      extraData: JSON.stringify({ fileId }),
    });
    return servicesLib.getJsonMessage(response);
  }

  public async deleteFile(fileId: string) {
    const response = await servicesLib.sendToQueue({
      queue: 'delete.file.id',
      messageBuffer: servicesLib.getBufferJson({
        fileId,
      }),
    });
    return servicesLib.getJsonMessage(response);
  }

};

const fileLib = new FileLib();
export default fileLib;
