// @ts-ignore
import PasswordValidator from 'password-validator';
import { hashSync, genSaltSync, compareSync } from 'bcrypt';

class PasswordLib {

  private schema: PasswordValidator
  private errors: string[]

  public constructor() {
    this.schema = new PasswordValidator();
    this.schema
      .is().min(8)
      .is().max(32)
      .has().uppercase()
      .has().lowercase()
      .has().digits()
      .has().symbols()
      .has().not().spaces();
  }

  public check(password: string, passwordConfirm: string): boolean {
    if (password != passwordConfirm) {
      this.errors = ['Different passwords'];
      return false;
    }

    this.errors = this.schema.validate(password, { list: true });
    return this.schema.validate(password);
  }

  public hash(password: string): string {
    return hashSync(password, genSaltSync());
  }

  public checkHash(password: string, hash: string): boolean {
    return compareSync(`${password}`, `${hash}`);
  }

  public getErrors(): string[] {
    return this.errors;
  }

};

const passwordLib = new PasswordLib();
export default passwordLib;
