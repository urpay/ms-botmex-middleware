import Axios, { AxiosInstance } from 'axios';
import { telegramBotUrl } from '../config/environment';

class WebHookLib {
  private api: AxiosInstance

  public constructor() {
    this.api = Axios.create({
      baseURL: telegramBotUrl,
    });
  }

  public async sendDayLiquidationToTelegramUser(telegramUser: string, result: number, amount: number) {
    if (!telegramUser) return;

    await this.api.post('/daily-result', { telegramUser, result, amount });
  }
}

export default new WebHookLib();
