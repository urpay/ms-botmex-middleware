import { envBitMex } from '../config/environment';
import crypto from 'crypto';
import Axios, { AxiosInstance } from 'axios';
import ErrorLib from './ErrorLib';
import * as returnMessages from '../config/returnMessages';
import { Client } from '../models/Client';

const apiUrl = envBitMex.apiUrl;

class BitMexLib {

  protected apiAxios: AxiosInstance;

  public constructor() {
    const headers = {
      'content-type': 'application/json',
      'Accept': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
    };

    this.apiAxios = Axios.create({
      baseURL: apiUrl,
      headers,
    });
  }

  public async post(client: any, path: string, body: any) {

    const { apiKey, apiSecret } = client;

    const expires = Math.round(new Date().getTime() / 1000) + 60;
    const signature = crypto.createHmac('sha256', apiSecret).update('POST' + path + expires + JSON.stringify(body)).digest('hex');
    const headers = {
      'api-expires': expires,
      'api-key': apiKey,
      'api-signature': signature,
    };

    try {
      return await this.apiAxios.post(path, body, { headers });
    } catch (error) {
      if (error?.response?.status && (error?.response?.status == 403 || error?.response?.status == 401)) {
        await Client.updateOne({ apiKey }, {
          apiIsActive: false,
        });
      }
      if (error.response?.status == 401) {
        throw new ErrorLib({
          message: returnMessages.ptBr.eBitMexNotAuth,
          httpCode: 401,
          errors: [
            `apiKey: ${client.apiKey}`,
          ],
        });
      }
      if (error.response?.status == 403) {
        throw new ErrorLib({
          message: returnMessages.ptBr.eBitMexDenied,
          httpCode: 403,
          errors: [
            `apiKey: ${client.apiKey}`,
          ],
        });
      }
      throw new ErrorLib({
        message: returnMessages.ptBr.eBitMexApi,
        errors: [
          error?.response?.data,
        ],
      });
    }

  }


  public async get(client: any, path: string, query?: any) {

    const { apiKey, apiSecret } = client;
    if (query) {
      query = query.replace(/:/g, '%3A');
      path = path + query;
    }

    const expires = Math.round(new Date().getTime()) + 60;

    const signature = crypto.createHmac('sha256', apiSecret).update('GET' + path + expires + '').digest('hex');
    const headers = {
      'api-expires': expires,
      'api-key': apiKey,
      'api-signature': signature,
    };

    try {
      return await this.apiAxios.get(path, {
        headers,
      });
    } catch (error) {
      if (error?.response?.status && (error?.response?.status == 403 || error?.response?.status == 401)) {
        await  Client.updateOne({ apiKey }, {
          apiIsActive: false,
        });
      }
      if (error?.response?.status == 401) {
        throw new ErrorLib({
          message: returnMessages.ptBr.eBitMexNotAuth,
          httpCode: 401,
          errors: [
            `apiKey: ${client.apiKey}`,
          ],
        });
      }
      if (error?.response?.status == 403) {
        throw new ErrorLib({
          message: returnMessages.ptBr.eBitMexDenied,
          httpCode: 403,
          errors: [
            `apiKey: ${client.apiKey}`,
          ],
        });
      }
      console.log(error.response.data);
      console.log(error.response.status);
      throw new ErrorLib({
        message: returnMessages.ptBr.eBitMexApi,
        errors: [
          error?.response?.data,
        ],
      });
    }
  }

  public async getWalletHistory(client: { apiKey: string, apiSecret: string }, options?: { count?: number }): Promise<any> {
    const count = options?.count || 500;

    return (await this.get(client, `/api/v1/user/walletHistory?currency=XBt&count=${count}&reverse=true`)).data;
  }

  public async getOrder(client: { apiKey: string, apiSecret: string }): Promise<any> {
    return (await this.get(client, '/api/v1/order?symbol=XBTUSD&count=500&reverse=true')).data;
  }

  public async getOpenOrder(client: { apiKey: string, apiSecret: string }): Promise<any> {

    return (await this.get(client, '/api/v1/order?symbol=XBTUSD&count=500&reverse=true&filter=%7B%22open%22%3A%22true%22%7D')).data;
  }

  public async getPosition(client: { apiKey: string, apiSecret: string }): Promise<any> {
    return (await this.get(client, '/api/v1/position')).data;
  }


  public async getWallet(client: { apiKey: string, apiSecret: string }): Promise<any> {
    return (await this.get(client, '/api/v1/user/wallet?currency=XBt')).data;
  }


  public async getUserMargin(client: { apiKey: string, apiSecret: string }): Promise<any> {
    return (await this.get(client, '/api/v1/user/margin?currency=XBt')).data;
  }

  public async getTradeHistory(client: { apiKey: string, apiSecret: string }, options?: { count?: number }): Promise<any> {
    const count = options?.count || 500;

    return (await this.get(client, `/api/v1/execution/tradeHistory?count=${count}&reverse=true`)).data;
  }

  public async orderClosePosition(client: { apiKey: string, apiSecret: string }, data: { symbol: string, price?: number }): Promise<any> {
    return (await this.post(client, '/api/v1/order/closePosition', data)).data;
  }
}


const bitMexLib = new BitMexLib();
export default bitMexLib;