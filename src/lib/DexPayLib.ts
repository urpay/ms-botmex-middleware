

import Axios, { AxiosInstance } from 'axios';
import ErrorLib from './ErrorLib';
import { envDexPay } from '../config/environment';
import querystring from 'querystring';

const { apiUrl, apiVersion, apiToken } = envDexPay;

class DexPayLib {

  protected apiAxios: AxiosInstance;

  public constructor() {
    const headers = {
      'content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization': `Bearer ${apiToken}`,
    };

    this.apiAxios = Axios.create({
      baseURL: `${apiUrl}/${apiVersion}`,
      headers,
    });
  }

  public async post(path: string, body: any): Promise<any> {

    try {
      return await this.apiAxios.post(path, body);
    } catch (error) {
      throw new ErrorLib({
        message: error.error,
      });
    }

  }


  public async get(path: string, query?: any): Promise<any> {
    const queryParams = '?' + querystring.stringify(query);
    try {
      return await this.apiAxios.get(path + queryParams);
    } catch (error) {
      throw new Error(error);
    }
  }

  public async saldoCarteiraBTC(): Promise<any> {
    return (await this.get('/wallet/btc/balance')).data;
  }

  public async listaEnderecos(): Promise<any> {
    return (await this.get('/wallet/btc/addresses')).data;
  }

  public async listaEnderecosSaldo(): Promise<any> {
    return (await this.get('/wallet/btc/addresses-balance')).data;
  }

  public async listaRecebidosEndereco(): Promise<any> {
    return (await this.get('/wallet/btc/received-by-address')).data;
  }

  public async criaEndereco(): Promise<any> {
    return (await this.get('/wallet/btc/new-address')).data;
  }

}


const dexPayLib = new DexPayLib();
export default dexPayLib;