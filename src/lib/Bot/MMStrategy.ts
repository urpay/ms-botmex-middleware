import { MainClass } from './Bot';
import bitMEXApi from './BitMEXAPI';

export default class MMStrategy {

  public static prepareOrder(index: number) {
    index++;
    let num = 30;
    let num2 = 0;
    let num3 = 30;
    let num4 = 3;
    let num5 = 30;
    let num6 = 6;

    
    let num7 = 0.0;

    if (index >= num2) {
      num7 += num * index;
    }
    if (index >= num4) {
      num7 += (num3 - num) * (index - num4);
    }
    if (index >= num6) {
      num7 += (num5 - num3) * (index - num6);
    }
    return num7;
  }

  public static async bitMexGetPosition() {
    try {
      console.log('getPosition...');
      const openPositions: any[] = await bitMEXApi.getOpenPositions(MainClass.pair);
      let num = 0;
      openPositions.forEach((item) => {
        num += item.currentQty;
      });
      console.log('getPosition: ' + num);
      return num;
    }
    catch (error) {
      console.log('getPosition::' + error);
      throw error;
    }
  }

  public static async bitMexGetPriceActual(type: string): Promise<any> {
    console.log('🚨');
    let num = -1.0;
    while (num <= 0.0) {
      try {
        const orderBook: any[] = await bitMEXApi.getOrderBook(MainClass.pair, 1);
        orderBook.forEach((item) => {
          if (item.side.toUpperCase() == type.toUpperCase()) {
            num = item.price;
            return;
          }
        });

      }
      catch
      {
        num = -1.0;
      }
    }
    return num;
  }


  public static async bitMexGetBalance(): Promise<any> {

    const { walletBalance } = (await bitMEXApi.getWallet())[0];
    console.log('walletBalance', walletBalance);
    return walletBalance / 100000000.0;
  }

  public static async bitMexGetPositionPrice() {
    try {
      const openPositions: any[] = await bitMEXApi.getOpenPositions(MainClass.pair);
      let num = 0.0;
      openPositions.forEach((item) => {
        num = item.avgEntryPrice;
      });
      console.log('getPositionPrice ' + num);
      return num;
    }
    catch (error) {
      console.log('getPositionPrice::' + error);
      return -1.0;
    }
  }
  public static async bitMexSetPosition() {
    try {
      MainClass.openPosition = 0;
      MainClass.positionPrice = 0.0;
      MainClass.liquidationPrice = 0.0;
      MainClass.leverage = 0.0;


      let openPositions: any[] = await bitMEXApi.getOpenPositions(MainClass.pair);
      openPositions.forEach((item) => {
        MainClass.openPosition = item.currentQty;
        MainClass.positionPrice = item.avgEntryPrice;
        MainClass.liquidationPrice = item.liquidationPrice;
        MainClass.leverage = item.leverage;
      });

      console.log('Data: ', new Date());
      console.log('POSITION: ' + MainClass.openPosition);
      console.log('PRICE POSITION: ' + MainClass.positionPrice);
      console.log('LIQUIDATION ' + MainClass.liquidationPrice);
      console.log('LEVERAGE ' + MainClass.leverage);
    }
    catch (error) {
      console.log('getPosition::' + error);
      throw new Error('Error getPosition');
    }
  }

  public static run(): void {


    setInterval(async () => {
      try {
        console.log('======================================');

        await this.bitMexSetPosition();
        //Pega posicao pela api
        MainClass.positionPrice = Math.round(MainClass.positionPrice);
        const openOrders = await bitMEXApi.getOpenOrders(MainClass.pair);
        MainClass.positionContracts = openOrders.length;
        console.log('Total de orders ' + openOrders.length);
        let str = '';
        let totalStep = MainClass.totalStep;
        if (MainClass.openPosition == 0) {
          if (openOrders.length != totalStep * 2 && (await this.bitMexGetPosition() == 0)) {
            console.log('Não temos posição aberta e também não temos as 20 ordens...');
            console.log('Vamos cancelar tudo!');
            await bitMEXApi.cancelAllOpenOrders(MainClass.pair);
            console.log('Canceladas');
            let priceActual = await this.bitMexGetPriceActual('Buy');
            console.log('Preço atual: ' + priceActual);
            let num: number = Math.round(await this.bitMexGetBalance() * 100.0 / 0.008);
            console.log('Total de contratosx: ' + num);
            let orderListBulk = [];
            for (let i = 0; i < totalStep; i++) {
              orderListBulk.push(bitMEXApi.limitBulk(MainClass.pair, 'Sell', num * (i + 1), priceActual + this.prepareOrder(i), true, 'Entry SHORT ' + (i + 1)));
              orderListBulk.push(bitMEXApi.limitBulk(MainClass.pair, 'Buy', num * (i + 1), priceActual - this.prepareOrder(i), true, 'Entry LONG ' + (i + 1)));
            }
            await bitMEXApi.ordersNewBulk(orderListBulk);
          }
        }
        else if (MainClass.openPosition < 0) {
          if (await this.bitMexGetPosition() < 0) {
            let flag = false;
            let text2 = '';
            let list2 = [];
            for (let j = 0; j < openOrders.length; j++) {
              if (openOrders[j].side == 'Buy' && openOrders[j].orderQty == Math.abs(MainClass.openPosition) && openOrders[j].price == Math.round(Math.abs(MainClass.positionPrice)) - 1.0) {
                flag = true;
              }
              if (openOrders[j].side == 'Buy') {
                list2.push(openOrders[j].orderID);
                text2 = text2 + '"' + openOrders[j].orderID + '",';
              }
            }
            if (!flag) {
              if (text2 != '') {
                console.log('Deletando ordens em LONG...');
                text2 = text2.substring(0, text2.length - 1);
                console.log('TEXT2', text2);
                await bitMEXApi.ordersDeleteBulk('[' + text2 + ']');
              }
              console.log('Criando um novo target em LONG para o position em SHORT');
              let num2 = 0.0;
              let num3 = 0;
              while (num2 <= 0.0) {
                num2 = Math.round(Math.abs(await this.bitMexGetPositionPrice()));
                num3++;
                console.log('avg Entry',num2);
                if (num3 > 50) {
                  throw new Error('Error ao pegar position price!');
                }
                
              }
              const _bitMexGetPosition = await this.bitMexGetPosition();
              await bitMEXApi.limit(MainClass.pair, 'Buy', Math.abs(_bitMexGetPosition), num2 - 1.0, true, 'TARGET LONG');
            }
          }
        }
        else if (MainClass.openPosition > 0 && (await this.bitMexGetPosition()) > 0) {
          let flag2 = false;
          let text3 = '';
          for (let k = 0; k < openOrders.length; k++) {
            if (openOrders[k].Side == 'Sell' && openOrders[k].OrderQty == Math.abs(MainClass.openPosition) && openOrders[k].Price == Math.round(Math.abs(MainClass.positionPrice)) + 1.0) {
              flag2 = true;
            }
            if (openOrders[k].Side == 'Sell') {
              text3 = text3 + '"' + openOrders[k].orderID + '",';
            }
          }
          if (!flag2) {
            if (text3 != '') {
              console.log('Deletando as ordens de SHORT...');
              text3 = text3.substring(0, text3.length - 1);
              await bitMEXApi.ordersDeleteBulk('[' + text3 + ']');
            }
            console.log('Criando um novo target em SHORT para o position em LONG');
            let num4 = 0.0;
            let num5 = 0;
            while (num4 <= 0.0) {
              num4 = Math.round(Math.abs(await this.bitMexGetPositionPrice()));
              num5++;
              if (num5 > 50) {
                throw new Error('Error ao pegar position price!');
              }
            }
            await bitMEXApi.limit(MainClass.pair, 'Sell', Math.abs(await this.bitMexGetPosition()), num4 + 1.0, true, 'TARGET SHORT');
          }
        }
        try {
          MainClass.webOBS = '';
          openOrders.forEach((item: any) => {
            MainClass.webOBS = MainClass.webOBS + item.OrdType + ' | ' + item.OrderQty + ' | ' + item.OrdStatus + ' | ' + item.Side + ' | ' + item.Price + ' | ' + item.Text + ' |<br/>';
          });
          if (MainClass.openPosition != 0) {
            MainClass.webOBS = MainClass.webOBS + '<b>Price Position:</b> ' + MainClass.positionPrice + '<br/>';
            MainClass.webOBS = MainClass.webOBS + '<b>Liquidation Price:</b> ' + MainClass.liquidationPrice + '<br/>';
          }
          MainClass.webOBS = MainClass.webOBS + 'Obs: ' + str;
        }
        catch (error) {
          console.log('🤔E', error);
        }

      }
      catch (ex) {
        console.log('while true::' + ex);
      }
    }, 16000);
  }

}
