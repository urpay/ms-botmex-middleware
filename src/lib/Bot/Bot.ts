import MMStrategy from './MMStrategy';

const jContainer = {
  'domain': 'https://www.bitmex.com',
  'key': 'SEp0h42o7MWgGWdv7W2tMSIY',
  'secret': 'gIohe5kCDx3YmWGch017EvGxWXf7Xb31torA_syq5p5lp8ym',
  'strategy': 'MM',
  'pair': 'XBTUSD',
  'priceStep': 30,
  'totalStep': 8,
  'interval': 600000,
  'webserver': 'enable',
  'webserverAutoOpen': 'enable',
  'webserverConfig': 'http://127.0.0.1:4000/bot/',
  'webserverIntervalCapture': 100000,
};

export class MainClass {
  public static version: string = '19.12.19';

  public static location: string;

  public static webOBS: string;

  public static bitmexKey: string;

  public static bitmexSecret: string;

  public static pair: string;

  public static timeFrame: string;

  public static strategy: string;

  public static qtdyContacts: number;

  public static priceStep: number;

  public static totalStep: number;

  public static profit: number;

  public static sizeProtect: number;

  public static leverage: number;

  public static noTradeHour1: number;

  public static noTradeHour2: number;

  public static step1: number;

  public static step2: number;

  public static stop: number;

  public static loopTrend: number;

  public static xRateLimitReset: number;

  public static interval: number;

  public static intervalCapture: number;

  public static positionContracts: number;

  public static openPosition: number;

  public static positionPrice: number;

  public static liquidationPrice: number;

  public static bitmexDomain: string;

  public static priceActual: number;;

  public static priceActualBRL: number;;

  public static stopGainDay: number;

  public static sizeArrayCandles: number;

  public static arrayPriceClose: number[];

  public static arrayPriceHigh: number[];

  public static arrayPriceLow: number[];

  public static arrayPriceVolume: number[];

  public static arrayPriceOpen: number[];

  public static arrayDate: Date[];

  public static jsonWallet: string;

  //bitMEXApi
  public static bitMEXApi: any;

  public static data: any;


  public static run() {
    
    this.init();
    try {
      console.log('Load config...');
      MainClass.bitmexKey = jContainer['key'];
      MainClass.bitmexSecret = jContainer['secret'];
      MainClass.bitmexDomain = jContainer['domain'];
      MainClass.pair = jContainer['pair'];
      MainClass.strategy = jContainer['strategy'];
      MainClass.priceStep = jContainer['priceStep'];
      MainClass.totalStep = jContainer['totalStep'];
      MainClass.interval = jContainer['interval'];
      MainClass.intervalCapture = jContainer['webserverIntervalCapture'];

      MMStrategy.run();

    }
    catch (error) {
      console.log('ERROR FATAL::' + error);
    }
  }

  public static init() {

    MainClass.webOBS = '';
    this.bitmexKey = '';
    this.bitmexSecret = '';
    this.pair = '';
    this.timeFrame = '5m';
    this.strategy = '';
    this.qtdyContacts = 0;
    this.priceStep = 0;
    this.totalStep = 0;
    this.profit = 0;
    this.sizeProtect = 8;
    this.leverage = 1.0;
    this.noTradeHour1 = 2;
    this.noTradeHour2 = 22;
    this.step1 = 0;
    this.step2 = 0;
    this.stop = 0;
    this.loopTrend = 60;
    this.xRateLimitReset = 0.0;
    this.interval = 0;
    this.intervalCapture = 0;
    this.positionContracts = 0;
    this.openPosition = 0;
    this.positionPrice = 0.0;
    this.liquidationPrice = 0.0;
    this.bitmexDomain = '';
    this.priceActual = 0.0;
    this.priceActualBRL = 0.0;
    this.stopGainDay = 1.0;
    this.sizeArrayCandles = 500;
    this.arrayPriceClose = [];
    this.arrayPriceHigh = [];
    this.arrayPriceLow = [];
    this.arrayPriceVolume =[];
    this.arrayPriceOpen = [];
    this.arrayDate = [];
    this.jsonWallet = '';
    this.bitMEXApi = null;
    this.data = {};
  }

}

MainClass.run();