import { envBitMex } from '../../config/environment';
import crypto from 'crypto';
import Axios, { AxiosInstance } from 'axios';
import querystring from 'querystring';
import { compareDesc } from 'date-fns';

// import ErrorLib from '../ErrorLib';
// import * as returnMessages from '../../config/returnMessages';

const apiUrl = envBitMex.apiUrl || 'https://www.bitmex.com';
const client = {
  apiKey: 'SEp0h42o7MWgGWdv7W2tMSIY',
  apiSecret: 'gIohe5kCDx3YmWGch017EvGxWXf7Xb31torA_syq5p5lp8ym',
};


class BitMEXApi {

  protected apiAxios: AxiosInstance;

  public constructor() {
    const headers = {
      'content-type': 'application/json',
      'Accept': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
    };

    this.apiAxios = Axios.create({
      baseURL: apiUrl,
      headers,
    });
  }

  public async post(path: string, body: any) {

    const { apiKey, apiSecret } = client;

    const expires = Math.round(new Date().getTime() / 1000) + 60;
    const signature = crypto.createHmac('sha256', apiSecret).update('POST' + path + expires + JSON.stringify(body)).digest('hex');
    const headers = {
      'api-expires': expires,
      'api-key': apiKey,
      'api-signature': signature,
    };
    try {
      return await this.apiAxios.post(path, JSON.stringify(body), { headers });
    } catch (error) {
      console.log(error.response.status);
      console.log(error.response);
      console.log(error.response.data);
      throw new Error(error.response.data);
    }

  }

  public async delete(path: string, body: any) {

    const { apiKey, apiSecret } = client;

    const expires = Math.round(new Date().getTime() / 1000) + 60;
    const query = body ? `?${querystring.stringify(body)}` : '';
    console.log(path + query);
    const signature = crypto.createHmac('sha256', apiSecret).update('DELETE' + path + query + expires).digest('hex');

    const headers = {
      'api-expires': expires,
      'api-key': apiKey,
      'api-signature': signature,
    };

    try {
      return await this.apiAxios.delete(path + query, { headers });
    } catch (error) {
      console.log(error.response.data);
      throw new Error(error.response.data);
    }

  }


  public async get(path: string, query?: any) {

    const { apiKey, apiSecret } = client;
    if (query) {
      query = query.replace(/:/g, '%3A');
      path = path + query;
    }

    const expires = Math.round(new Date().getTime()) + 60;

    const signature = crypto.createHmac('sha256', apiSecret).update('GET' + path + expires + '').digest('hex');
    const headers = {
      'api-expires': expires,
      'api-key': apiKey,
      'api-signature': signature,
    };

    try {
      return await this.apiAxios.get(path, {
        headers,
      });
    } catch (error) {

      console.log(error.response.data);
      process.exit();

      throw new Error(error.response.data);
    }
  }

  public async getWallet(): Promise<any> {
    return (await this.get('/api/v1/user/walletHistory?currency=XBt')).data;
  }
  public async getOpenPositions(symbol: string): Promise<any> {
    const openPositions = (await this.get('/api/v1/position')).data;
    return openPositions
      .filter((position: any) => position.symbol == symbol)
      .sort((current: any, next: any) => compareDesc(current.timestamp, next.timestamp));
  }

  public async getOrderBook(symbol: string, depth: number): Promise<any> {
    return (await this.get(`/api/v1/orderBook/L2?symbol=${symbol}&depth=${depth}`)).data;
  }



  public async getOpenOrders(symbol: string): Promise<any> {
    return (await this.get(`/api/v1/order?symbol=${symbol}&filter=%7B%22open%22%3A%22true%22%7D`)).data;
  }
  public async cancelAllOpenOrders(note: string): Promise<any> {
    console.log(note);
    return (await this.delete('/api/v1/order/all', { symbol: 'XBTUSD', text: 'note' })).data;
  }

  public limitBulk(symbol: string, side: string, orderQty: number, price: number, force = false, text = 'nothing'): Promise<any> {
    if (price <= 0.0) {
      throw new Error('Price incorrect!!');
    }
    let dictionary: any = {
      symbol,
      side,
      orderQty,
      ordType: 'Limit',
      text,
    };

    if (!force) {
      dictionary['execInst'] = 'ParticipateDoNotInitiate';
    }
    dictionary['price'] = Math.abs(Math.round(price));
    return dictionary;
  }

  public async ordersNewBulk(orders: any[]): Promise<any> {
    try {
      const res = await this.post('/api/v1/order/bulk', { orders });
      return res;

    } catch (e) {
      console.log(e);

    }

  }

  public async ordersDeleteBulk(orderID: any): Promise<any> {
    return (await this.delete('/api/v1/order', { orderID })).data;
  }
  public async limit(symbol: string, side: string, orderQty: number, price: number, force = false, text = ''): Promise<any> {

    console.log('symbol', symbol);
    console.log('side', side);
    console.log('orderQty', orderQty);
    console.log('price', price);
    console.log('force', force);
    console.log('text', text);

    if (price <= 0.0) {
      throw new Error('Price incorrect!!');
    }
    let dictionary = {
      symbol,
      side,
      orderQty: Math.abs(orderQty),
      ordType: 'Limit',
      text,
      execInst: 'Close',
      price: Math.abs(Math.round(price)),
    };

    return (await this.post('/api/v1/order', dictionary)).data;

  }


}


const bitMEXApi = new BitMEXApi();
export default bitMEXApi;