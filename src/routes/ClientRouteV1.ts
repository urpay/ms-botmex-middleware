import express from 'express';
import rateLimit from '../middlewares/rate';
import clientTokenController from '../controllers/ClientApi/TokenController';
import authMiddleware from '../middlewares/authClient';
import clientController from '../controllers/ClientApi/ClientController';
import walletController from '../controllers/ClientApi/WalletController';
import twoFactorsController from '../controllers/ClientApi/TwoFactorsController';
import marginController from '../controllers/ClientApi/MarginController';
import cancelOrderController from '../controllers/ClientApi/CancelOrderController';
import paymentController from '../controllers/ClientApi/PaymentController';



const RoutesClientV1 = express.Router();


RoutesClientV1.post(
  '/login',
  rateLimit('client-login'),
  clientTokenController.generateTokenLoginRoute,
);

RoutesClientV1.get(
  '/client',
  authMiddleware,
  clientController.show,
);

RoutesClientV1.get(
  '/client/two-factors/secret',
  rateLimit('two-factors-get-secret', 1),
  authMiddleware,
  twoFactorsController.getSecret,
);
  
RoutesClientV1.put(
  '/client/two-factors/active',
  rateLimit('two-factors-set-active', 3),
  authMiddleware,
  twoFactorsController.setActive,
);

RoutesClientV1.put(
  '/client-edit',
  authMiddleware,
  clientController.update,
);

RoutesClientV1.put(
  '/client-edit-password',
  authMiddleware,
  clientController.updatePassword,
);

RoutesClientV1.get(
  '/operations-month',
  authMiddleware,
  walletController.getOperationsMonths,
);

RoutesClientV1.get(
  '/operations-year',
  authMiddleware,
  walletController.getOperationsYears,
);
  
RoutesClientV1.get(
  '/wallet-history',
  authMiddleware,
  walletController.walletHistory,
);

RoutesClientV1.get(
  '/open-transaction',
  authMiddleware,
  walletController.openWalletTranscation,
);

RoutesClientV1.get(
  '/amountMonth',
  authMiddleware,
  walletController.amountMonth,
);

RoutesClientV1.get(
  '/open-transaction',
  authMiddleware,
  walletController.openWalletTranscation,
);


RoutesClientV1.get(
  '/userMargin',
  authMiddleware,
  marginController.userMargin,
);


RoutesClientV1.post(
  '/stopLoss',
  authMiddleware,
  cancelOrderController.setStopLoss,
);


RoutesClientV1.get(
  '/stopLoss',
  authMiddleware,
  cancelOrderController.getStopLoss,
);

RoutesClientV1.get(
  '/payments',
  paymentController.list,
);

export default RoutesClientV1;
