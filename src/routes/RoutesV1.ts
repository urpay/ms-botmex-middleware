import express from 'express';

import userController from '../controllers/UserController';
import tokenController from '../controllers/TokenController';

import authMiddleware from '../middlewares/auth';
import emailController from '../controllers/EmailController';
import rateLimit from '../middlewares/rate';
import twoFactorsController from '../controllers/TwoFactorsController';
import userPasswordRecoveryController from '../controllers/PasswordRecoveryController';
import clientController from '../controllers/ClientController';
import walletController from '../controllers/WalletController';
import marginController from '../controllers/MarginController';
import paymentController from '../controllers/PaymentController';
import TradeHistoryController from '../controllers/TradeHistoryController';


const RoutesV1 = express.Router();


//Usuário

RoutesV1.post(
  '/user',
  authMiddleware,
  rateLimit('register'),
  userController.store,
);

RoutesV1.post(
  '/login',
  rateLimit('login'),
  tokenController.generateTokenLoginRoute,
);

RoutesV1.get(
  '/user',
  authMiddleware,
  userController.show,
);

RoutesV1.put(
  '/user/update-password',
  authMiddleware,
  userController.updatePassword,
);

RoutesV1.get(
  '/user/email/send-validation',
  authMiddleware,
  emailController.sendValidationRoute,
);

RoutesV1.post(
  '/user/email/check-validation',
  rateLimit('email-check-validation'),
  authMiddleware,
  emailController.checkValidationRoute,
);


// dois fatores
RoutesV1.get(
  '/user/two-factors/secret',
  rateLimit('two-factors-get-secret', 1),
  authMiddleware,
  twoFactorsController.getSecret,
);

RoutesV1.put(
  '/user/two-factors/active',
  rateLimit('two-factors-set-active', 3),
  authMiddleware,
  twoFactorsController.setActive,
);

RoutesV1.post('/user/password-recovery/send-email',
  rateLimit('post-user-password-recovery-send-email', 1),
  userPasswordRecoveryController.sendCodeRecoveryEmail,
);

RoutesV1.put('/user/password-recovery/check-code-email',
  rateLimit('post-user-password-recovery-check-code-email'),
  userPasswordRecoveryController.checkCodeEmail,
);

RoutesV1.put('/user/password-recovery/set-email-password',
  rateLimit('put-user-password-recovery-set-email-password', 3),
  userPasswordRecoveryController.setNewPasswordEmail,
);


// Cliente
RoutesV1.post(
  '/client',
  authMiddleware,
  clientController.store,
);

RoutesV1.get(
  '/client/:clientId',
  authMiddleware,
  clientController.show,
);

RoutesV1.get(
  '/clients',
  authMiddleware,
  clientController.list,
);

RoutesV1.put(
  '/client/:clientId',
  authMiddleware,
  clientController.update,
);

RoutesV1.put(
  '/client/:clientId/updatePassword',
  authMiddleware,
  clientController.updatePassword,
);

RoutesV1.put(
  '/client/:clientId/update-api-status',
  authMiddleware,
  clientController.updateApiStatus,
);


//Cliente Wallet

RoutesV1.get(
  '/client/:clientId/walletHistory',
  authMiddleware,
  walletController.walletHistory,
);

RoutesV1.get(
  '/client/:clientId/trade-history',
  authMiddleware,
  TradeHistoryController.index,
);

RoutesV1.get(
  '/client/:clientId/open-transaction',
  authMiddleware,
  walletController.openWalletTranscation,
);

RoutesV1.get(
  '/client/:clientId/amountMonth',
  authMiddleware,
  walletController.amountMonth,
);

RoutesV1.get(
  '/client/:clientId/getOrder',
  walletController.getOrder,
);

RoutesV1.get(
  '/client/:clientId/operations-year',
  walletController.getOperationsYears,
);

RoutesV1.get(
  '/client/:clientId/operations-month',
  walletController.getOperationsMonths,
);

//Cliente margin
RoutesV1.get(
  '/client/:clientId/userMargin',
  authMiddleware,
  marginController.userMargin,
);


RoutesV1.get(
  '/payments',
  paymentController.list,
);





export default RoutesV1;
