import express from 'express';
import StatisticsController from '../controllers/PublicApi/StatisticsController';

const PublicRoutes = express.Router();

PublicRoutes.get('/statistics/total-clients', StatisticsController.getTotalClients);
PublicRoutes.get('/statistics/clients-btc-sum', StatisticsController.getBTCClientsSum);
PublicRoutes.get('/statistics/operations', StatisticsController.getMinAndMaxOperations);
PublicRoutes.get('/statistics/market-quotation', StatisticsController.getMarketQuotation);
PublicRoutes.get('/statistics/success-risk', StatisticsController.sucessRisk);

PublicRoutes.get('/statistics/daily-result', StatisticsController.dailyLiquidation);

export default PublicRoutes;
