
import { Client, ClientInterface } from '../models/Client';
import walletTransactionService from '../services/WalletTransactionService';
import bitMexLib from '../lib/BitMexLib';


export const getWalletTransactions = async () => {
  const clientList = await Client.find({ isActive: true, apiIsActive: true }).exec();

  clientList.forEach(async (client: ClientInterface) => {
    try {
      const response = await bitMexLib.getWalletHistory(client);

      await walletTransactionService.updateWalletTransactionList(client._id, response);
    } catch (error) {
      console.log(error);
    }
  });

};
