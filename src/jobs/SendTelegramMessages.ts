
import { Client, ClientInterface } from '../models/Client';
import { WalletTransactionInterface, WalletTransaction } from '../models/WalletTransaction';
import WebHookLib from '../lib/WebHookLib';


export const dailyLiquidation = async () => {
  const clientList = await Client.find({ isActive: true, apiIsActive: true }).exec();
  clientList.forEach(async (client: ClientInterface) => {
    const wallet: WalletTransactionInterface =
      await WalletTransaction.findOne({ clientId: client._id });

    try {
      if (client?.telegramId) {
        await WebHookLib.sendDayLiquidationToTelegramUser(client.telegramId, wallet.result, wallet.amount / 100000000);
      }
    } catch (error) {
      console.log(error);
    }
  });
};
