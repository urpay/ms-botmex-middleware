import cron from 'node-cron';
import { getClientsWallets } from './GetClientsWallets';
import { getWalletTransactions } from './GetWalletTransactions';
import { getUnrealised } from './GetUnrealisedOrder';
import { processPayments } from './ProcessPayment';
import { generatePayments } from './GeneratePayments';
import { dailyLiquidation } from './SendTelegramMessages';
import { getClientsTradeHistory } from './GetTradeHistory';

cron.schedule('0 0 */8 * * *', async () => {
  console.log('GET ALL WALLETS ', new Date());
  await getClientsWallets();
});

cron.schedule('10 */1 * * *', async () => {
  console.log('GET ALL WALLET TRANSACTIONS ', new Date());
  await getWalletTransactions();
});

cron.schedule('20 */1 * * *', async () => {
  console.log('SENDING TELEGRAM MESSAGES', new Date());
  await dailyLiquidation();
});

cron.schedule('*/10 * * * * *', async () => {
  await getUnrealised();
});

cron.schedule('20 */1 * * *', async () => {
  await generatePayments();
});

cron.schedule('*/10 * * * *', async () => {
  await processPayments();
});

cron.schedule('*/5 * * * *', async () => {
  await getClientsTradeHistory();
});