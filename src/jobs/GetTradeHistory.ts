import bitMexLib from '../lib/BitMexLib';
import { ClientInterface, Client } from '../models/Client';
import TradeHistoryService from '../services/TradeHistoryService';

export const getClientsTradeHistory = async () => {
  const clientList = await Client.find({ isActive: true, apiIsActive: true }).exec();
  clientList.forEach(async (client: ClientInterface) => {
    try {
      const clientTrades = await bitMexLib.getTradeHistory(client);
      await TradeHistoryService.createOrUpdateTradeHistory(client._id, clientTrades);
    } catch (error) {
      console.log(error);
    }
  });
};
