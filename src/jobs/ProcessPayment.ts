


import { Payment, PaymentInterface } from '../models/Payment';
import dexPayLib from '../lib/DexPayLib';

interface ReceivedInterface {
  address: string;
  amount: number;
  confirmations: number;
  label: string;
  txids: string[];
}
export const processPayments = async () => {

  const payments = await Payment.find({ status: 'pending' });
  payments.forEach(async (payment: PaymentInterface) => {
    try {
      const receiveds: ReceivedInterface[] = await dexPayLib.listaRecebidosEndereco();
      const receivedConfirmed = receiveds.find((received: ReceivedInterface) => received.address == payment.walletAddress && received.confirmations == 3);
      if (!receivedConfirmed) { return; }
      payment.update({
        status:'processed',
      });
    } catch (error) {
      throw new Error(error);
    }
  });
};