import { Client, ClientInterface } from '../models/Client';
import { Payment, RealisedTransaction } from '../models/Payment';
import { toDate, set } from 'date-fns';
import { WalletTransaction, WalletTransactionInterface } from '../models/WalletTransaction';
import dexPayLib from '../lib/DexPayLib';
import paymentService from '../services/PaymentService';

export const generatePayments = async (paymentDay?: Date) => {

  paymentDay = paymentDay || new Date();
  const clientList = await Client.find({}).exec();

  clientList.forEach(async (client: ClientInterface) => {
    try {

      const startDay = toDate(
        set(
          paymentDay,
          {
            hours:0,
            minutes:0,
            seconds:0,
            milliseconds:0,
          }),
      );
      const endDay = toDate(
        set(
          paymentDay,
          {
            hours:23,
            minutes:59,
            seconds:59,
            milliseconds:999,
          }),
      );

      //Busca pagameto que se inicia hoje
      const paymentToday = await Payment.findOne({
        clientId: client._id,
        billingDate: {
          $gte: startDay,
          $lte: endDay,
        },
      });

      if (paymentToday) { return; }

      const walletTransaction: WalletTransactionInterface = await WalletTransaction.findOne({
        transactTime: {
          $gte: startDay,
          $lte: endDay,
        },
        transactType: 'RealisedPNL',
        clientId: client._id,
      }).exec();

      if (!walletTransaction) { return; }

      console.log(walletTransaction);

      const {
        transactID,
        transactTime,
        amount,
      } = walletTransaction.toJSON();

      const realised: RealisedTransaction = {
        transactID,
        transactTime,
        amount,
        rate: walletTransaction.rate?.rate,
      };

      if (!paymentToday) {
        process.exit();
        const walletAddress = await dexPayLib.criaEndereco();
        await paymentService.create({
          clientId: client._id,
          amount: walletTransaction?.result,
          rate: walletTransaction?.rate?.value,
          billingDate: paymentDay,
          dueDate: endDay,
          walletAddress: walletAddress.address,
          walletDescription: 'dex',
          walletSource: 'dex',
          realised,
          walletTransaction: walletTransaction._id,
        });
      }
    } catch (error) {
      throw new Error(error);
    }
  });

};