
import { CancelOrder } from '../models/CancelOrder';
import bitMexLib from '../lib/BitMexLib';
import ErrorLib from '../lib/ErrorLib';

export const getUnrealised = async () => {
  const cancelOrderList = await CancelOrder.find({}).populate('clientId');

  cancelOrderList.forEach(async (cancelOrder: any) => {

    if (!cancelOrder.isActive) {
      return;
    }

    try {

      if (cancelOrder.clientId.apiIsActive === false) { 
        return;
      }

      const [currentPosition] = await bitMexLib.getPosition(cancelOrder?.clientId);
      if (!currentPosition) {
        return;
      }

      if (currentPosition?.unrealisedPnl == 0) {
        return;
      }

      const stopLoss = Math.abs(cancelOrder.stopLoss) / -100;

      const { walletBalance } = await bitMexLib.getUserMargin(cancelOrder.clientId);

      if (!walletBalance) {
        return;
      }

      const unrealisedPct = currentPosition.unrealisedPnl / walletBalance;


      if (stopLoss < unrealisedPct) {
        return;
      }

      if (currentPosition?.unrealisedRoePcnt < 0) {
        console.log('Cancel position ', currentPosition.unrealisedRoePcnt);

        const positionClose = await bitMexLib.orderClosePosition(cancelOrder.clientId, {
          symbol: 'XBTUSD',
        });
        console.log(positionClose);
      }


    } catch (error) {
      console.log(error);
    }





  });
};

