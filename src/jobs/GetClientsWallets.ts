
import { Client, ClientInterface } from '../models/Client';
import WalletService from '../services/WalletService';
import bitMexLib from '../lib/BitMexLib';


export const getClientsWallets = async () => {
  const clientList = await Client.find({ isActive: true, apiIsActive: true }).exec();
  clientList.forEach(async (client: ClientInterface) => {
    try {
      const response = await bitMexLib.getWallet(client);
      await WalletService.createOrUpdate({ clientId: client._id, ...response });
    } catch (error) {
      console.log(error);
    }
  });
};

