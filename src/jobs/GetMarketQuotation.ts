
import marketQuotationService from '../services/MarketQuotationService';
import blockchainLib from '../lib/BlockchainLib';


export const getMarketQuotation = async () => {
  const marketQuotation = await blockchainLib.getMarketQuotation();
  await marketQuotationService.createOrUpdate(marketQuotation['USD']);
};


