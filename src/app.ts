// seta as variaveis de ambiente
import './bootstrap';


import './lib/Mail/consumers';

// importa as crons
import './jobs/Crons';


// configuração do server express
import expressServer from './config/server';

// rotas v1
import RoutesV1 from './routes/RoutesV1';
import RoutesClientV1 from './routes/ClientRouteV1';
import PublicRoutes from './routes/PublicRoutesV1';


//aplica as rotas ao servidor no path /v1
expressServer.applyRoute('/v1', RoutesV1);
expressServer.applyRoute('/v1-client', RoutesClientV1);
expressServer.applyRoute('/v1-public', PublicRoutes);

// inicia o servidor
expressServer.initServer();


