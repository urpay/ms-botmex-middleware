import { config } from 'dotenv';

config({
  path: process.env.NODE_ENV == 'test' ? '.env.test' : '.env',
});

export const envIsDev = () => process.env.NODE_ENV == 'development' ? true : false;
export const envIsTest = () => process.env.NODE_ENV == 'test' ? true : false;
export const envNotRate = () => process.env.NOT_RATE == 'true' ? true : false;

export const envAppName = process.env.APP_NAME;

export const envServer = {
  portHttp: process.env.PORT_HTTP || 80,
  portHttps: process.env.PORT_HTTPS || 443,
  privkey: (process.env.HTTPS_PRIVKEY_PEM || '').replace(/\\n/g, '\n'),
  fullchain: (process.env.HTTPS_FULLCHAIN_PEM || '').replace(/\\n/g, '\n'),
  domain: process.env.SERVER_DOMAIN,
  domainUrl: `${envIsDev() || envIsTest() ? 'http' : 'https'}://${process.env.SERVER_DOMAIN}`,
};

export const envRedis = {
  host: process.env.REDISDB_HOST,
  port: Number(process.env.REDISDB_PORT),
  pass: process.env.REDISDB_PASS,
  tlsCa: process.env.REDISDB_CERTIFICATE_BASE64 ? Buffer.from(process.env.REDISDB_CERTIFICATE_BASE64, 'base64').toString() : undefined,
};

export const envMongodb = {
  uri: process.env.MONGODB_URI,
  sslCa: process.env.MONGODB_CERTIFICATE_BASE64 ? [Buffer.from(process.env.MONGODB_CERTIFICATE_BASE64, 'base64').toString()] : undefined,
};

export const envSendgrid = {
  apiKey: process.env.SENDGRID_API_KEY,
  email: process.env.SENDGRID_EMAIL,
  category: process.env.SENDGRID_CATEGORY,
};

export const envBitMex = {
  apiUrl: process.env.BITMEX_API_URL,
};

export const envBlockchain = {
  apiUrl: process.env.BLOCKCHAIN_API_URL,
};

export const envDexPay = {
  apiToken: process.env.DEXPAY_TOKEN,
  apiUrl: process.env.DEXPAY_API_URL,
  apiVersion: process.env.DEXPAY_API_VERSION,
};


export const telegramBotUrl = process.env.TELEGRAM_BOT_URL;


export const envTokenUser = process.env.TOKEN_ENV_USER;
export const envTokenClient = process.env.TOKEN_CLIENT_USER;
