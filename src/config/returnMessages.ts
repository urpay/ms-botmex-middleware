export const ptBr = {

  // Error messages
  eDataNotSent: 'Dados exigidos não enviados ou inválidos.',

  eDataBase: 'Erro no banco de dados.',

  ePasswordInvalid: 'Senha inválida.',

  eInValidation: 'Erro na validação.',


  eInSendFile: 'Erro no envio do arquivo.',
  fileSaved: 'Arquivo enviado e salvo com sucesso.',

  eEmailIsChecked: 'Seu e-mail já foi validado.',
  eEmailNotChecked: 'Seu e-mail não está validado.',
  eEmaiUsed: 'Este já é o seu e-mail atual.',
  eEmaiExist: 'Este e-mail já está cadastrado.',
  eEmaiExistChange: 'Este e-mail está reservado para processo de mudança.',
  eEmailIsSent: (time: number) => `Seu e-mail já foi enviado. Tente novamente em ${time} segundos.`,

  eTokenNotSent: 'Token não enviado.',
  eTokenInvalid: 'Token inválido.',
  eTokenEmailInvalid: 'Este link de validação de e-mail é inválido.',
  eRequestMaxReached: 'Você atingiu o número máximo de requisições.',


  eCodeInvalid: 'Seu código está inválido.',
  eCodeValid: 'Seu código está válido.',
  eCodeAlreadyExist: 'Foi enviado um código de verificação recentemente.',

  eTwoFactorsIsActive: 'Seu código de 2 fatores já está ativo.',
  eTwoFactorsNoActive: 'Seu código de 2 fatores não está ativo.',
  eTwoFactorsNoSecret: 'Seu código de 2 fatores ainda não gerou um código secreto.',
  eTwoFactorsIsLastUsed: 'Seu código de 2 fatores foi alterado recentemente.',
  eTwoFactorsInvalid: 'Seu código de 2 fatores está incorreto',

  // Success messages
  registerComplete: 'Cadastro completado com sucesso.',
  emailSent: 'E-mail enviado com sucesso.',
  emailChecked: 'Seu e-mail foi validado com sucesso.',
  twoFactorsActived: 'Seu código de 2 fatores foi ativado com sucesso.',
  dataUpdated: 'Dados atualizados.',
  codeSent: 'Código enviado com sucesso.',
  passwordChanged: 'Senha alterada com sucesso.',


  //Users messages
  eUserNotFound: 'Usuário não cadastrado.',
  eInvalidUserId: 'ID inválido.',

  //Client Messages
  eClientNotFound: 'Cliente não cadastrado.',
  clientNotExist: 'Cliente não cadastrado',


  eCancelOrderNotFoud: 'Cliente não tem ordem cadastrada.',



  //Bitmex Erro

  eBitMexNotAuth: 'Chave BOTMEX não autorizada',
  eBitMexDenied: 'ERRO BOTMEX negada',
  eBitMexApi: 'Erro api BITMEX',
};
