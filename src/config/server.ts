import { envServer, envIsDev, envIsTest } from './environment';

import './database/mongodb';

import express, { NextFunction, Response, Request, ErrorRequestHandler } from 'express';
import helmet from 'helmet';
import cors from 'cors';
import { Server } from 'http';
import { createServer } from 'https';
import { isIP } from 'validator';

// @ts-ignore
import Youch from 'youch';
import 'express-async-errors';
import ErrorLib from '../lib/ErrorLib';

export default new class ExpressServer {

  private server: express.Express
  private http: Server
  private https: Server

  public constructor() {
    this.server = express();
    this.http = new Server(this.server);
    this.https = createServer({
      key: envServer.privkey,
      cert: envServer.fullchain,
    }, this.server);

    //proxy
    this.server.set('trust proxy', true);

    this.middlewares();
    this.routes();
  }

  private middlewares() {
    //helmet
    this.server.use(helmet());

    //json
    this.server.use(express.json());

    //cors
    this.server.use(cors());

    //reject ip access
    this.server.use((req, res, next) => {
      if (!isIP(req.get('host').split(':')[0]) || envIsDev() || envIsTest()) {
        next();
      } else {
        res.status(403).end();
      }
    });

    //reject change ip access
    this.server.use((req, res, next) => {
      if (req.ips.length !== 1 && !envIsDev() && !envIsTest()) {
        res.status(403).end();
      } else {
        next();
      }
    });

    //force https
    this.server.use((req, res, next) => {
      if (req.secure || envIsDev() || envIsTest()) {
        next();
      } else {
        res.redirect(`https://${req.get('host')}${req.url}`);
      }
    });
  }

  private routes() {
    //rota robots.txt
    this.server.get('/robots.txt', (req, res) => {
      res.type('text/plain');
      res.send('User-agent: *\nDisallow: /');
    });
  }

  private exceptionHandler() {
    this.server.use(async (err: ErrorRequestHandler, req: Request, res: Response, next: NextFunction) => {
      // Vai processar um erro interno da api
      // @ts-ignore
      const possibleErrorLib: ErrorLib = err;
      if (possibleErrorLib.isErrorLib && possibleErrorLib.isErrorLib()) {
        res.status(possibleErrorLib.getHttpCode() || 422).json(possibleErrorLib.getErrorJson());
        return;
      }

      // vai processar de maneira generica pois foi um erro de execulção
      const errors = await new Youch(err, req).toJSON();

      if (!envIsDev() && !envIsTest())
        delete errors.error.frames;

      res.status(500).json(errors);
      return;
    });
  }

  /**
   * Iniciar os servidores http e https
   */
  public initServer() {
    //handler errors
    this.exceptionHandler();

    //start http
    this.http.listen(envServer.portHttp, () => {
      console.log(`HTTP: start port ${envServer.portHttp}`);
    });

    //start https production
    if (!envIsDev() && !envIsTest())
      this.https.listen(envServer.portHttps, () => {
        console.log(`HTTPS: start port ${envServer.portHttps}`);
      });
  }

  /**
   * Fecha os servidor http e https
   */
  public closeServer() {
    //close http
    this.http.close();

    //close https
    if (!envIsDev() && !envIsTest())
      this.https.close();
  }

  public applyRoute(routePath: string, route: express.Router) {
    this.server.use(routePath, route);
  }
};
