import '../../src/app';
import expressServer from '../../src/config/server';
import { envServer } from '../../src/config/environment';
import dbMongo from '../../src/config/database/mongodb';

//nao e usado a opção do supertest de fazer os testes sem startar o servidor pois com isso ele nao trata os erros globais do express
import request from 'supertest';

describe('User', () => {

  //antes de cada teste deleta o banco de dados
  beforeEach(async () => {
    await dbMongo.getMongoose().connection.dropDatabase();
  });

  //depois de todos os testes deleta o banco de dados e fecha o servidor
  afterAll(async () => {
    await dbMongo.getMongoose().connection.dropDatabase();
    expressServer.closeServer();
  });

  it('O usuario deve ser cadastrado', async () => {
    const response = await request(envServer.domainUrl)
      .post('/v1/user')
      .send({
        user: 'RODRIGUES COSTA',
        name: 'JOSE RODRIGUES DA COSTA NETO',
        document: '01193515289',
        email: 'rodriguescosta@urpay.com.br',
        password: 'sb..3Dois1',
        passwordConfirm: 'sb..3Dois1',
      });

    expect(response.body).toHaveProperty('user');
  });

});
