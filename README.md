# ms-middleware

Lista de erros

0: Dados obrigatórios não enviados.
// Algum campo necessário não foi enviado na requisição

1: Dado repetido no banco da dados.
// Este dado não pode ser repetido e algum usuário já está utilizando-o. Como documento, e-mail ou telefone.

2: Dado inválido para a API.
// Dado enviado não está na formatação adequada. Como letras em um campo de documento.

3: Senhas de confirmação diferem.
// No cadastro de usuário, se os campos de senha e de confirmação são diferentes, este erro é chamado.

4: Usuário ou senha inválido.
// Dados para o login inválidos.

5: Requisição efetuada sem token de autenticação.
// O token não foi enviado.

6: Token inválido.
//Não deve ser confundido com o código de dois fatores inválido (erro 9).

7: Dois fatores inativo.
// Endpoint necessita dos dois fatores, mas a conta do usuário está com ele inativo.

8: Código de dois fatores inválido pois expirou.
// O código de dois fatores enviado é o anterior e portanto um novo já foi gerado.

9: Código de dois fatores inválido.
// O código está inválido.

10: Código de dois fatores está ativo e acesso ao secret não é mais permitido.

11: O secret não foi gerado.
// Como o secret não foi gerado, o código de dois fatores não pode ser ativado.

12: O item já está validado.
// Erro apresentado quando está tentando validar e-mail ou telefone e o mesmo já está validado.


14: Código de verificação inválido.
// O código verificação do e-mail ou telefone está inválido.

15: O dado que está sendo inserido já pertence ao usuário.
// Quando o usuário está querendo atualizar o e-mail ou o número de telefone que já está cadastrado para ele.

16: Documento não cadastrado.
// Erro apresentado quando a consulta de documento retorna nenhum usuário correspondente.

17: Muitas Requisições
// Erro apresentado quando tem muitas requisições sobre um endpoint

18: Erro no processamento / upload
// Erro apresentado quando da erro no envio da imagem para o microserviço ms-files

19: Erro na operação
//Erro apresentado quando ocorre o erro na operação entre contas.

20: Usuário não encontrado.

21: Telefone não esta valido

22: Já tentou recuperar a senha o numero maximo de vezes no telefone
